require('./config/passportConf');
require("dotenv").config();
require("./db/db.js");
const mongoose = require('mongoose');
const db = mongoose.connection;
const path = require('path');
db.on("error", console.error.bind(console, "connection error: "));
db.once("open", function () {
  console.log("Connected successfully");
});


const express = require('express');
const userRouter = require("./routers/user.js");
const vgRouter = require("./routers/videogames.js");
const rentRouter = require("./routers/rent");
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const app = express();


app.use(cors());
app.use(express.json());
app.use(bodyParser.json());
app.use(userRouter);
app.use(vgRouter);
app.use(rentRouter);
app.use(passport.initialize())
app.use('/images', express.static(path.join("backend/images")));


app.listen(4000, ()=>{
  console.log(`server @ http://localhost:4000`);
});

