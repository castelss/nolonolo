const express = require('express');
const User = require('../models/user');
const router = new express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require("bcrypt");
const jwtHelper = require('../config/jwtHelper');
const Videogame = require('../models/videogames')


router.get('/users/', jwtHelper.verifyJwtToken, async(req, res, next) => {
    User.findOne({ _id: req.userData._id }).then(user => {
        if (user) {        
            res.status(200).json({
                message: "Profile fetched successfully!",
                user: user
            });
        } else {
            res.status(404).json({ message: "Profile not found!" });
        }
    });
});

router.get('/users/msg', jwtHelper.verifyJwtToken, async(req, res, next) => {
    User.findOne({ _id: req.userData._id }).then(user => {
        if (user) {        
            res.status(200).send(user.messaggi)
        } else {
            res.status(404).json({ message: "Profile not found!" });
        }
    });
});



router.post('/users/registration', async(req, res)=>{
    const user = new User(req.body);
    /*if (!user.name){
        res.status(400).json({message: "Nome mancante"})
    }*/
    ruolo = "user"
    user.punti = 0
    const token = jwt.sign(
        { user_id: user._id,
            ruolo: ruolo },
        "TecWeb",
        {
            expiresIn: "2m",
        }
        );
        user.token = token;
    try{
        user.save(function(err) {
            if (err) throw err;
        })
        res.status(201).send(user);
    } catch (error) {
        res.status(500).send(error);
    }
});


/**
 * Now we can make a post request – /api/authenticate with email and password.
 * If there is a user with given credential then jwt token can be received at client side. 
 * Normally we save this token in browser local storage because we need the token in order to access private routes from Node JS API.
 */

router.post('/users/login',async(req, res, next)=>{
    // call for passport authentication
    try{
        const { email, password } = req.body;
        //console.log(password)
        if (!(email || password)) {
            return res.status(400).json({
                message: "Inserire tutte le credenziali"
              })
        }


        const user = await User.findOne({ email });
    
        
        //console.log(pwCheck)
        if (user){
            if(user.stato=="bloccato"){
                return res.json({
                    message: "Sei stato bloccato dall'admin"
                  });
            }
            const pwCheck =await bcrypt.compareSync(password, user.password)
            //console.log(password)
            // Create token
            if(pwCheck){ 
                if(user.punti>=200 && user.punti<400){
                    var discount = 5
                    User.findOneAndUpdate({ '_id': user._id }, {'sconto' : discount},function(err, doc) {
                        if (err) return res.send(500, {error: err});
                    });
                } else if(user.punti>=400 && user.punti<600){
                    var discount = 12
                    User.findOneAndUpdate({ '_id': user._id }, {'sconto' : discount},function(err, doc) {
                        if (err) return res.send(500, {error: err});
                    });
                } else if(user.punti>=600 && user.punti<800){
                    var discount = 18
                    User.findOneAndUpdate({ '_id': user._id }, {'sconto' : discount},function(err, doc) {
                        if (err) return res.send(500, {error: err});
                    });
                } else if(user.punti>=800 && user.punti<1000){
                    var discount = 24
                    User.findOneAndUpdate({ '_id': user._id }, {'sconto' : discount},function(err, doc) {
                        if (err) return res.send(500, {error: err});
                    });
                } else if(user.punti>=1000){
                    var discount = 30
                    User.findOneAndUpdate({ '_id': user._id }, {'sconto' : discount},function(err, doc) {
                        if (err) return res.send(500, {error: err});
                    });
                }
                const token = jwt.sign(
                { user_id: user._id, 
                    email: user.email },
                "TecWeb",
                {
                    expiresIn: "5m",
                }
                );
                user.token = token;
            // user
                res.status(200).json(user);
            } else res.status(400).json({
                message: "ERRORE: Password errata"
              });
        } else res.status(400).json({
                message: "ERRORE: Email Errata"
              });
        
    }catch (error) {
        console.log(error);
    }
});

router.get('/users/allUsers', async(req,res)=>{
    try {
        const users = await User.find({});
        res.status(200).send(users);
    } catch (error) {
        res.status(500).send(error);
    }
})
router.post('/users/lockUSer', async(req,res,next)=>{
    User.findOneAndUpdate({ '_id': req.body._id }, {'stato' : "bloccato"},{new : true},function(err, doc) { 
        if(err){
            return res.status(500).send(err)
        } else
            return res.status(200).send(doc);
    });
})
router.post('/users/unlockUSer', async(req,res,next)=>{
    User.findOneAndUpdate({ '_id': req.body._id }, {'stato' : "attivo"},{new : true},function(err, doc) {
        if(err){
            return res.status(500).send(err)
        } else
            return res.status(200).send(doc);       
    });
})
router.post('/users/update',jwtHelper.verifyJwtToken, async(req,res,next)=>{
    nome= req.body.nome
    cognome= req.body.cognome
    username = req.body.username
    dataNascita = req.body.dataNascita 
    update = {'nome':nome, 'cognome':cognome, 'username': username, 'dataNascita' : dataNascita}
    User.findOneAndUpdate({ '_id': req.userData._id }, update,{new : true},function(err, doc) {
        if(err){
            return res.status(500).send(err)
        } else
            return res.status(200).send(doc);       
    });
})

router.post('/users/admin/update', async(req,res,next)=>{
    User.findById({_id : req.body.id}).then(user=>{
        if(user){
          var update = {
            'nome': req.body.nome != "" ? req.body.nome : user.nome, 
            'cognome': req.body.cognome != "" ? req.body.cognome : user.cognome, 
            'dataNascita': req.body.dataNascita != "" ? req.body.dataNascita : user.dataNascita, 
            'username': req.body.username != "" ? req.body.username : user.username,
            'punti': req.body.azzera != "" ? 0 : user.punti,
            'ruolo': req.body.ruolo != "" ? req.body.ruolo : user.ruolo
          }
          User.findOneAndUpdate({ _id: req.body.id }, update,{new : true},function(err, doc) {
            if (err) return res.send(err);
            return res.status(200).json({
              message: "utente modificato",
              doc: doc,
            })
          })
        }else
          return res.send(err)
      })
    
})
router.post('/users/admin/msg', async(req,res,next)=>{
    User.findById({_id : req.body.id}).then(user=>{
        if(user){
          var msg = {
            'testo': req.body.testo
          }
          User.findOneAndUpdate({ _id: req.body.id }, {$push:{'messaggi':msg}},{new : true},function(err, doc) {
            if (err) return res.send(err);
            return res.status(200).json({
              message: "utente modificato",
              doc: doc,
            })
          })
        }else
          return res.send(err)
      })
    
})
router.post('/users/updatePw',jwtHelper.verifyJwtToken, async(req,res,next)=>{
    old= req.body.old
    newPw= req.body.new
    User.findOne({ '_id': req.userData._id }).then( async user =>{
        if(user){
            const pwCheck = await bcrypt.compareSync(old, user.password);
            if (pwCheck){
                const salt = await bcrypt.genSalt(10);
                // now we set user password to hashed password
                newPw = await  bcrypt.hash(newPw, salt);
                User.updateOne({ '_id': user._id }, { 'password': newPw },function(err, doc) {
                    if(err){
                        return res.status(500).send(err);
                    } else
                        return res.status(200).send(doc);       
                });
            } else
                return res.status(500);
        } else 
            return res.status(500);
    })

})

router.get('/users/getWishlist', jwtHelper.verifyJwtToken,  async(req, res, next)=>{
    //console.log(req.userData._id)
    User.findOne({ _id: req.userData._id }).populate({path: 'preferiti',model : Videogame}).then(user => {
        if (user) {        
            res.status(200).json({
                videogame: user.preferiti
            });
        } else {
            res.status(404).json({ message: "Profile not found!" });
        }
    });
    
})



module.exports = router;

