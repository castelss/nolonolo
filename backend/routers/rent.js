const express = require('express');
const Rent = require('../models/rent');
const jwt = require('jsonwebtoken');
const jwtHelper = require('../config/jwtHelper')
const Videogame = require('../models/videogames');
const User = require('../models/user');
const { update } = require('lodash');
const router = new express.Router();

router.post('/videogames/disponibilita', async(req, res, next) => {
    const videogioco = await Videogame.findById(req.body.videogioco).exec();
    var check = true;
    
    const token = req.headers.authorization.split(" ")[1];
    const decodedToken = jwt.verify(token, "TecWeb");

    var dataInizio = new Date();
    dataInizio= Date.parse(req.body.dataInizio);
    var dataFine = new Date();
    dataFine= Date.parse(req.body.dataFine);
    var durata = (dataFine-dataInizio)/(1000 * 60 * 60 * 24);
    var weeks = durata/7;
    
    
    Rent.find({ videogioco: videogioco._id }).populate({path:'videogioco', model: Videogame}).then(rents => {
        
        /**
         * funziona: trova TUTTI I NOLEGGI DEL GIOCO
         *           INSERISCE I NOLEGGI     
         */
       // console.log(rent.length)
        if (rents.length > 0) {

            rents.forEach(elem => {
                if ((elem.dataInizio.getTime() <= dataInizio && dataInizio < elem.dataFine.getTime())){
                    check = false; 
                }else if((elem.dataInizio.getTime() <= dataFine && dataFine < elem.dataFine.getTime())){
                    check = false; 
                }      
            });
            
            if (check){
                try{
                    const rent = new Rent(req.body);
                    const points = 50+(10*weeks);
                    if(weeks >= 5){
                        var sconto = 0.50;
                    }else{
                        var sconto = weeks * 0.05;
                    }
                    rent.prezzoPrevisto = videogioco.pzFissoBaby + weeks * [videogioco.pzFissoBaby-(videogioco.pzFissoBaby*sconto)];
                    rent.user = decodedToken.user_id;
                    rent.durata = weeks;
                    rent.stato = "confermato";
                    rent.save(function(err) {
                        User.findOneAndUpdate({ '_id': decodedToken.user_id }, {$inc: {'punti' : points}},function(err, doc) {
                            if (err) return res.send(500, {error: err});
                            
                        });
                        Videogame.findOneAndUpdate({ '_id': videogioco._id }, {$inc: {'noleggi' : 1}},function(err, doc) {
                            if (err) return res.send(500, {error: err});
                        });
                        if (err) throw err;
                    })
                    res.status(200).json({
                        message: "noleggio disponibile",
                        code: 1,
                        rent: req.body,
                    })
                } catch (error) {
                    res.status(500).send({message: error});
                };   
            }else{
                res.json({
                    message: "noleggio non disponibile cambiare date",
                    code: 2
                });
            }            
        } else {
            
            try{
                const rent = new Rent(req.body);
                
                if(weeks >= 5){
                    var sconto = weeks * 0.50;
                }else{
                    var sconto = weeks * 0.05;
                }
                rent.prezzoPrevisto = videogioco.pzFissoBaby + weeks * [videogioco.pzFissoBaby-(videogioco.pzFissoBaby*sconto)];
                rent.stato = "confermato";
                rent.user = decodedToken.user_id;
                rent.durata = weeks;
                const points = 50+(10*weeks);
                
                rent.save(function(err) {
                    User.findOneAndUpdate({ '_id': decodedToken.user_id }, {$inc: {'punti' : points}},function(err, doc) {
                        if (err) return res.send(500, {error: err});
                        //return res.send('Succesfully saved.');
                    });
                    Videogame.findOneAndUpdate({ '_id': videogioco._id }, {$inc: {'noleggi' : 1}},function(err, doc) {
                        if (err) return res.send(500, {error: err});
                    });                       
                    if (err) throw err;
                })
                res.status(200).json({
                    message: "noleggio disponibile",
                    code: 1,
                    rent: req.body,
                })
            } catch (error) {
                res.status(500).send({message: error});
            }; 
        }
    });

    
});

router.get('/noleggi', jwtHelper.verifyJwtToken,  async(req, res, next)=>{
    Rent.find({ user: req.userData._id }).populate({path:'videogioco', model: Videogame}).then(rents => {
        if (rents) {  
            rents.forEach(rent => {
                Rent.findById({_id : rent.id }).then(rent => {
                    if (rent) {
                        if(rent.stato != 'attivo' || rent.stato != "pagato"){
                            const today = new Date();
                            today.setUTCHours(00,00,00,0000);
                            if (today.getTime()==rent.dataInizio.getTime()){
                                Rent.findOneAndUpdate({ '_id': rent._id }, {'stato' : "attivo"},{new : true},function(err, doc) {
                                    if (err) return res.send(500, {error: err});
                                    return console.log('Succesfully saved.');
                                });
                            }
                        }
                    } else {
                        res.json({ message: "Dettagli non disponibili" });
                    }
                });
            });         
            res.status(200).json({
                message: "Noleggi trovati",
                rents: rents
            });
        } else {
            res.json({ message: "Non hai ancora effettuato noleggi" });
        }
    });
}); 
router.post('/noleggi/delete', jwtHelper.verifyJwtToken, async(req, res, next)=>{
    console.log(req.body)
    const rent = await Rent.findById(req.body._id).populate({path:'videogioco', model: Videogame}).exec();
    Videogame.findOneAndUpdate({ '_id': rent.videogioco._id }, {$inc: {'noleggi' : -1}},function(err, doc) {
        if (err) return res.send(500, {error: err});
    });
    Rent.deleteOne({ '_id': req.body._id }, function(err, doc) {
        if (err) return res.send(err);
        return res.status(200).json({
            message: "prenotazione cancellata",
            doc: doc,
        })
    });

})
// jwtHelper.verifyJwtToken
router.post('/noleggi/modifica', async(req, res, next)=>{
    //console.log(req.body)
    const rent = await Rent.findById(req.body.id).populate({path:'videogioco', model: Videogame}).exec();
    //console.log(rent)
    var check = true;

    var dataInizio = new Date();
    dataInizio= Date.parse(req.body.dataInizio);
    var dataFine = new Date();
    dataFine= Date.parse(req.body.dataFine);
    var durata = (dataFine-dataInizio)/(1000 * 60 * 60 * 24);
    var weeks = durata/7;
    
    
    Rent.find({ videogioco: rent.videogioco._id }).populate({path:'videogioco', model: Videogame}).then(rents => {
        
        /**
         * funziona: trova TUTTI I NOLEGGI DEL GIOCO
         *           INSERISCE I NOLEGGI     
         */
       // console.log(rent.length)
        if (rents.length > 0) {

            rents.forEach(elem => {
                if ((elem.dataInizio.getTime() <= dataInizio && dataInizio < elem.dataFine.getTime())){
                    check = false; 
                }else if((elem.dataInizio.getTime() <= dataFine && dataFine < elem.dataFine.getTime())){
                    check = false; 
                }      
            });
            console.log(check)
            if (check){                   
                if(weeks >= 5){
                    var sconto = weeks * 0.50;
                }else{
                    var sconto = weeks * 0.05;
                }
                prezzoPrevisto = rent.videogioco.pzFissoBaby + weeks * [rent.videogioco.pzFissoBaby-(rent.videogioco.pzFissoBaby*sconto)];
                durata = weeks;
                stato = "confermato";
                console.log(req.body.dataFine)
                var update = {'dataInizio':req.body.dataInizio , 'dataFine' : req.body.dataFine,
                'prezzoPrevisto':prezzoPrevisto, 'durata' : durata, 'stato' : stato}
                Rent.findOneAndUpdate({_id : req.body.id}, update , {new : true}, function(err, doc){
                    if (err) return res.send(err);
                    return res.status(200).json({
                        message: "prenotazione modificata",
                        doc: doc,
                    })
                })
            }else{
                res.json({
                    message: "noleggio non disponibile cambiare date",
                    code: 2
                });
            }            
        } else {
            if(weeks >= 5){
                var sconto = weeks * 0.50;
            }else{
                var sconto = weeks * 0.05;
            }
            prezzoPrevisto = rent.videogioco.pzFissoBaby + weeks * [rent.videogioco.pzFissoBaby-(rent.videogioco.pzFissoBaby*sconto)];
            durata = weeks;
            stato = "confermato";
            var update = {'dataInizio':req.body.dataInizio , 'dataFine' : req.body.dataFine,
            'prezzoPrevisto':prezzoPrevisto, 'durata' : durata, 'stato' : stato}
            Rent.findOneAndUpdate({_id : req.body.id}, update , {new : true}, function(err, doc){
                if (err) return res.send(err);
                return res.status(200).json({
                    message: "prenotazione modificata",
                    doc: doc,
                })
            }) 
        }
    });

})

router.post('/admin/modifica', async(req, res, next)=>{
    //console.log(req.body)
    const rent = await Rent.findById(req.body.id).populate({path:'videogioco', model: Videogame}).exec();
    //console.log(rent)
    var check = true;

    var dataInizio = new Date();
    dataInizio= Date.parse(req.body.dataInizio);
    var dataFine = new Date();
    dataFine= Date.parse(req.body.dataFine);
    var durata = (dataFine-dataInizio)/(1000 * 60 * 60 * 24);
    var weeks = durata/7;
    
    
    Rent.find({ videogioco: rent.videogioco._id }).populate({path:'videogioco', model: Videogame}).then(rents => {
        
        /**
         * funziona: trova TUTTI I NOLEGGI DEL GIOCO
         *           INSERISCE I NOLEGGI     
         */
       // console.log(rent.length)
        if (rents.length > 0) {
                               
                if(weeks >= 5){
                    var sconto = weeks * 0.50;
                }else{
                    var sconto = weeks * 0.05;
                }
                prezzoPrevisto = rent.videogioco.pzFissoBaby + weeks * [rent.videogioco.pzFissoBaby-(rent.videogioco.pzFissoBaby*sconto)];
                durata = weeks;
                stato = "confermato";
                console.log(req.body.dataFine)
                var update = {'dataInizio':req.body.dataFine , 'dataFine' : req.body.dataFine,
                'prezzoPrevisto':prezzoPrevisto, 'durata' : durata, 'stato' : stato}
                Rent.findOneAndUpdate({_id : req.body.id}, update , {new : true}, function(err, doc){
                    if (err) return res.send(err);
                    return res.status(200).json({
                        message: "prenotazione modificata",
                        doc: doc,
                    })
                })      
        } else {
            if(weeks >= 5){
                var sconto = weeks * 0.50;
            }else{
                var sconto = weeks * 0.05;
            }
            prezzoPrevisto = rent.videogioco.pzFissoBaby + weeks * [rent.videogioco.pzFissoBaby-(rent.videogioco.pzFissoBaby*sconto)];
            durata = weeks;
            stato = "confermato";
            var update = {'dataInizio':req.body.dataInizio , 'dataFine' : req.body.dataFine,
            'prezzoPrevisto':prezzoPrevisto, 'durata' : durata, 'stato' : stato}
            Rent.findOneAndUpdate({_id : req.body.id}, update , {new : true}, function(err, doc){
                if (err) return res.send(err);
                return res.status(200).json({
                    message: "prenotazione modificata",
                    doc: doc,
                })
            }) 
        }
    });

})


router.get('/noleggi/:id', jwtHelper.verifyJwtToken,  async(req, res, next)=>{

    //console.log(req.userData._id)
    Rent.findById({ _id : req.params.id }).populate({path:'videogioco', model: Videogame}).populate({path:'user', model: User} ).then(rent => {
        if (rent) {
            //console.log(rent.user)
            res.status(200).json({
                message: "Dettagli disponibili",
                rent: rent
            });
        } else {
            res.json({ message: "Dettagli non disponibili" });
        }
    });
}); 

router.get('/admin/noleggi',  async(req, res, next)=>{
    Rent.find({}).populate({path:'videogioco', model: Videogame}).then(rents => {
    if (rents) {  
        rents.forEach(rent => {
            Rent.findById({_id : rent.id }).then(rent => {
                if (rent) {
                    //console.log(rent)
                    if(rent.stato != 'attivo' || rent.stato != "pagato"){
                        const today = new Date();
                        today.setUTCHours(00,00,00,0000);
                        if (today.getTime()>=rent.dataInizio.getTime() && today.getTime()<=rent.dataFine.getTime()){
                            Rent.findOneAndUpdate({ '_id': rent._id }, {'stato' : "attivo"},{new : true},function(err, doc) {
                                if (err) return res.send(500, {error: err});
                                return console.log('Succesfully saved.');
                            });
                        }
                    }
                } else {
                    res.json({ message: "Dettagli non disponibili" });
                }
            });
        });         
        res.status(200).send(rents)
    } else {
        res.json({ message: "Non hai ancora effettuato noleggi" });
    }
})
}); 
router.get('/admin/noleggi/:id',  async(req, res, next)=>{
    Rent.findById({ _id : req.params.id }).populate({path:'videogioco', model: Videogame}).populate({path:'user', model: User} ).then(rent => {
        if (rent) {
            res.status(200).json({
                
                message: "Dettagli disponibili",
                rent: rent
            });
        } else {
            res.json({ message: "Dettagli non disponibili" });
        }
    });
}); 

router.post('/admin/fattura', async(req, res, next) => {
    console.log(req.body.id)
     var mora
     var sconto
     var metodo
    Rent.findById({ _id : req.body.id }).populate({path:'videogioco', model: Videogame}).populate({path:'user', model: User} ).then(rent => {
        if (rent) {
            switch (req.body.metodo) {
                case '1':
                    metodo = "carta";
                  break;
                case '2':
                    metodo = "contanti";
                  break;
            }
            switch (req.body.sconto) {
                case '1':
                    sconto = 0;
                  break;
                case '2':
                    sconto = rent.user.sconto;
                  break;
            }
            switch (req.body.mora) {
                case '1':
                    mora = 0;
                  break;
                case '2':
                    mora = 5;
                  break;
                case '3':
                    mora = 7;
                  break;
                case '4':
                    mora = 10;
                  break;
                case '5':
                    mora = 12;
                  break;
                case '6':
                    mora = 15;
                  break;
            }
            switch (req.body.stato) {
                case '1':
                    rent.videogioco.stato = "Buono";
                  break;
                case '2':
                    rent.videogioco.stato = "Scarso";
                  break;
                case '3':
                    rent.videogioco.stato = "Ottimo";
                  break;
            }
            totale = rent.prezzoPrevisto + mora - sconto;
            var update = {'fattura.metodo':metodo, 'fattura.sconto': sconto, 'fattura.dataPagamento': new Date(),
                    'fattura.totale': totale, 'fattura.nota' : req.body.nota, 'fattura.mora':mora, 'stato': "pagato"}
            Rent.updateOne({ '_id': rent._id }, update ,function(err, doc) {
                if(err){
                    
                    return res.status(500).send(err);
                } else
                    return res.status(200).send(doc);       
            });
           
        } else {
            res.json({ message: "noleggio non trovato" });
        }
    });

});


module.exports = router