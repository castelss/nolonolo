const express = require('express');
const VideoGame = require('../models/videogames');
const router = new express.Router();
var multer = require('multer');
const jwtHelper = require('../config/jwtHelper')
const User = require('../models/user');
const { result, update } = require('lodash');

const MIME_TYPE_MAP = {  
  'image/png': 'png',  
  'image/jpeg': 'jpg',  
  'image/jpg': 'jpg'  
}; 

var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './backend/images');
  },
  filename: (req, file, cb) => {
    const name = file.originalname.toLowerCase().split(' ').join('_');  
    
    cb(null, name);  
  }
});

var upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: (req, file, cb) => {
    if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
      cb(null, true);
    } else {
      cb(null, false);
      return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
    }
  }
});



router.post('/videogames/upload',upload.single('image'), async(req,res,next)=>{
  const url = req.protocol + '://'+ req.get("host");  
  const videogame = new VideoGame(req.body);


  switch (videogame.categoria) {
    case '1':
      videogame.categoria = "Avventura";
      break;
    case '2':
      videogame.categoria = "Sport";
      break;
    case '3':
      videogame.categoria = "Musica";
      break;
    case '4':
      videogame.categoria = "Educazione";
      break;
  }

  switch (videogame.conservazione) {
    case '1':
      videogame.conservazione = "Buono";
      break;
    case '2':
      videogame.conservazione = "Scarso";
      break;
    case '3':
      videogame.conservazione = "Ottimo";
      break;
  }

  videogame.image = url + "/images/" + req.file.filename  ;
  //console.log(videogame.image)
  videogame.save().then(result=>{
    res.status(200).json({
      message: "videogioco inserito",
      videogame: result._id,
    })
  })

});

router.post('/videogames/review',jwtHelper.verifyJwtToken, async(req,res,next)=>{
  user_id = req.userData._id
  VideoGame.findById({_id : req.body.id}).then(videogame=>{
    if (videogame){
      recensione= {'testo': req.body.testo, 'stelle': req.body.stars, 'user': user_id}
      VideoGame.findOneAndUpdate({ _id: videogame._id }, {$push:{'recensione':recensione}},{new : true},function(err, doc) {
        if (err) return res.send(err);
        return res.status(200).json({
          message: "recensione inserita",
          doc: doc,
        })
    });
    }else{
      return res.status(404).json({
        message: "videogioco non trovato"
      })
    }
  })
});

router.post('/videogames/update', async(req,res,next)=>{
  VideoGame.findById({_id : req.body.id}).then(videogame=>{
    if(videogame){
      var update = {
        'nome': req.body.nome != "" ? req.body.nome : videogame.nome, 
        'categoria': req.body.categoria != "" ? req.body.categoria : videogame.categoria, 
        'conservazione': req.body.conservazione != "" ? req.body.conservazione : videogame.conservazione, 
        'pzFissoBaby': req.body.pzFissoBaby != "" ? req.body.pzFissoBaby : videogame.pzFissoBaby
      }
    
         
      VideoGame.findOneAndUpdate({ _id: req.body.id }, update,{new : true},function(err, doc) {
        if (err) return res.send(err);
        return res.status(200).json({
          message: "videogioco modificato",
          doc: doc,
        })
      })
    }else
      return res.send(err)
  })
  
    
});
router.post('/videogames/cambiaStato', async(req,res,next)=>{
  VideoGame.findById({_id : req.body.id}).then(videogame=>{
    if(videogame){
      var update = {
        'disponibile': videogame.disponibile == true ? false : true
      }
    
         
      VideoGame.findOneAndUpdate({ _id: req.body.id }, update,{new : true},function(err, doc) {
        if (err) return res.send(err);
        return res.status(200).json({
          message: "videogioco modificato",
          doc: doc,
        })
      })
    }else
      return res.send(err)
  })
  
    
});

router.post('/videogames/delete', async(req,res,next)=>{
  //console.log(req.body)
  VideoGame.deleteOne({ '_id': req.body.id }, function(err, doc) {
    if (err) return res.send(err);
    return res.status(200).json({
        message: "videogioco cancellata",
        doc: doc,
    })
});
  
    
});


router.get('/videogames', async(req,res)=>{
    try {
        const videogames = await VideoGame.find({});
        res.status(200).send(videogames);
    } catch (error) {
        res.status(500).send(error);
    }
})

router.get('/videogames/top3', async(req,res)=>{
  try {
      const videogames = await VideoGame.find({}).sort({'noleggi': -1});
      res.status(200).send(videogames);
  } catch (error) {
      res.status(500).send(error);
  }
})

router.get('/videogames/:id', async(req, res, next) => {
    VideoGame.findById({
        _id:req.params.id
    }).populate({path:'recensione.user', model: User})
    .exec((err, videogame) => {
        if (err) {
          res.json({
            success: false,
            message: "Videogame is not found",
          });
        } else {
          if (videogame) {
            res.json({
              success: true,
              videogame: videogame,
            });
          }
        }
      });
});

router.post('/videogames/wishlist',jwtHelper.verifyJwtToken, async(req,res)=>{
  user_id = req.userData._id
  videogioco = req.body
  User.findOneAndUpdate({ _id: user_id }, {$push:{'preferiti':videogioco._id}},{new : true},function(err, doc) {
    if (err) return res.send(err);
    return res.status(200).json({
      message: "wishlist modificata",
      user: doc,
    })
  });
});
router.post('/videogames/deleteWishlist',jwtHelper.verifyJwtToken, async(req,res)=>{
  user_id = req.userData._id
  videogioco = req.body
  User.findOneAndUpdate({ _id: user_id }, {$pull:{'preferiti':videogioco._id}},{new : true},function(err, doc) {
    if (err) return res.send(err);
    return res.status(200).json({
      message: "wishlist modificata",
      user: doc,
    })
  });
});



module.exports = router;