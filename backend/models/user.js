const mongoose = require("mongoose");
const bcrypt = require("bcrypt");


const userSchema = new mongoose.Schema({
    nome: String,
    cognome: String,
    username: { type: String},
    email: { type: String, index: { unique: true } },
    ruolo: {type: String, default: "user"},
    stato: {type: String, default: "attivo"},
    password: String,
    dataNascita: Date,
    punti: {type: Number, default: 0},
    sconto: {type: Number, default: 0},
    totnolegi: {type: Number, default: 0},
    preferiti: [
        {
          type: mongoose.Schema.Types.ObjectId,
          ref: "Videogame"
        }
    ],
    messaggi: [{testo : String}],
    token:String,
}, {
    timestamps: true
});



userSchema.pre('save', function (next) {
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(this.password, salt, (err, hash) => {
            this.password = hash;
            this.saltSecret = salt;
            next();
        });
    });
});


const User = mongoose.model('User', userSchema);
module.exports = User;




