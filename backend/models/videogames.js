const mongoose = require("mongoose");
const vgSchema = new mongoose.Schema({
    pzFissoBaby: Number,
    noleggi: {type: Number, default :0},
	nome: String,
	descrizione: String,
    image: String,
	disponibile: {type: Boolean, default: true},
	conservazione: String,
    categoria: String,
    recensione: [{
        user:{
            type: mongoose.Schema.Types.ObjectId,
            ref: "User"
          },
        testo: String,
        stelle: Number
    }]
}, {
    timestamps: true
});

const VideoGame = mongoose.model('VideoGame', vgSchema);
module.exports = VideoGame;