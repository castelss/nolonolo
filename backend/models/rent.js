const mongoose = require("mongoose");
const User = require('./user')
const Videogame = require('./videogames')

const rentSchema = new mongoose.Schema({
    user:{
          type: mongoose.Schema.Types.ObjectId,
          ref: "User"
        }
      ,
    dataInizio: Date,
    dataFine: Date,
    durata: {type: Number, default: 0},
    prezzoPrevisto: {type: Number, default:0},
    videogioco:
        {
          type: mongoose.Schema.Types.ObjectId,
          ref: "Videogame"
        },
    stato: {type: String, default: "in avvio"},
    fattura:{
        metodo: {type: String, default:""},
        dataPagamento: {type: Date, default: null},
        totale: {type: Number, default:0},
        sconto: {type: Number, default:0},
        mora: {type: Number, default:0},
        nota: {type: String, default: "Nessuna nota"},
    }
}, {
    timestamps: true
});


const Rent = mongoose.model('Rent', rentSchema);
module.exports = Rent;




