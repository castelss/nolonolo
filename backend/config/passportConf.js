const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const mongoose = require('mongoose');
require('../models/user');

var User = mongoose.model('User');

/**
 * use function from passport will configure the middleware. Inside the function, an object of localStrategy is a passed with 
 * one option and a call back function.
 * For option, we mention usernameField = email. because by default local strategy uses username and password but 
 * for this application we will use email instead of username.
 * Inside the callback function, we have defined user authentication criteria for both success and failure 
 * with the help of findOne function from mongoose. this callback function will get executed when actual authenticate function 
 * from passport is called.
 */


passport.use(
    new localStrategy({ usernameField: 'email' },
        (username, password, done) => {
            User.findOne({ email: username },
                (err, user) => {
                    if (err)
                        return done(err);
                    // unknown user
                    else if (!user)
                        return done(null, false, { message: 'E-mail non presente nel sistema.' });
                    // wrong password
                    else if (!user.verifyPassword(password))
                        return done(null, false, { message: 'Password Sbagliata.' });
                    // authentication succeeded
                    else
                        return done(null, user);
                });
        })
);