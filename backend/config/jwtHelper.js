const jwt = require('jsonwebtoken');

module.exports.verifyJwtToken = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        //console.log(token)
        const decodedToken = jwt.verify(token, "TecWeb");
        //console.log(decodedToken)
        req.userData = { email: decodedToken.email, _id: decodedToken.user_id };
        //console.log(req.userData)
        next();
      } catch (error) {
        res.status(401).json({ message: "Auth failed!" });
      }
}