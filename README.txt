COMPONENTI: Elena Forieri (0000880637) - Giorgia Castelli (0000873787)

Istruzioni per l'installazione:
1)Scaricare la repository 'nolonolo' da BitBucket (https://bitbucket.org/castelss/nolonolo/src/main/)
2)Aprire un terminale
3)Posizionarsi in nolonolo
4)Eseguire il comando npm install
5)Al termine dell'installazione, eseguire il comando npm run dev
6)Aprire un altro terminale
7)Posizionarsi in nolonolo/my-app
8)Eseguire il comando npm install
9)Al termine dell'installazione,eseguire il comando ng serve
10)Aprire una nuova scheda in Chrome (o in qualsiasi altro browser) e andare all'indirizzo: http://localhost:4200/

Note sul database:
I dati vengono già memorizzati all'interno di un cluster di MongoDB visualizzabile su mongo db compass tramite il seguente link mongodb+srv://admin:admin@nolonolo.ck3y7.mongodb.net/test

Credenziali per accedere come amministratore:
E-MAIL: admin@nolonolo.com
PASSWORD: password

Credenziali per accedere come utente:
E-MAIL: eri@nolo.com
PASSWORD: password

Descrizione delle funzionalità implementate:
Nolonolo è un'applicazione web commissionata da un negozio specializzato in videogiochi a realtà virtuale. I clienti possono iscriversi alla piattaforma fornendo le proprie generalità. Ogni cliente ha diritto ad una fidelity card virtuale in cui raccogliere punti. Per ogni noleggio effettuato, i punti sulla carta aumentano. I punti cumulati sulla carta determinano lo status del cliente nella piattaforma: baby, pro o genius. I clienti visualizzano prezzi fissi diversi per lo stesso videogioco a seconda del livello cui appartengono. Tutti gli utenti possono accedere al catalogo e selezionare un videogioco per scoprirne i dettagli. Se loggati possono: fargli una recensione, inserirlo nella lista dei desideri o noleggiarlo. Il noleggio può iniziare immediatamente o in una data futura in base alle preferenze del cliente stesso e/o in base alla disponibilità del videogioco. I clienti, a noleggio scaduto, devono recarsi in uno dei negozio fisici per pagare il prezzo dovuto definito come prezzo fisso del videogioco + prezzo variabile basato sulla durata del noleggio stesso. Sono state definite delle soglie punti che, se raggiunte, danno diritto al cliente alla detrazione di una quota dal prezzo totale. Se il noleggio viene concluso in ritardo, il cliente deve pagare anche un surplus. Lo stesso vale se il videogioco è stato riportato in condizioni peggiori rispetto a quando noleggiato. Chi chiude il noleggio è l'amministratore, ovvero il dipendente del negozio, che si occupa attivamente di inserire le informazioni necessarie per concludere lo stesso e comunicare il totale dovuto al cliente. Il cliente, tramite la sua area privata, può in qualsiasi momento visualizzare lo storico e futuro dei noleggi effettuati sulla piattaforma e, per ciascuno, conoscerne e talvolta modificarne i dettagli. Se il noleggio è già stato chiuso, i dettagli includono anche le informazioni di fatturazione. Se il noleggio è confermato ma non attivo, il cliente può richiederne la cancellazione o la modifica (della data di inizio e di fine). Il cliente può inoltre ricevere delle notifiche dallo staff.

OPERAZIONE COMUNI A CLIENTI E AMMINISTRATORI: 
Autenticazione alla piattaforma
Disconnessione alla piattaforma
Visualizzazione del catalogo dei videogiochi
Visualizzazione dei dettagli di ciascun videogioco

OPERAZIONI RISERVATE AI CLIENTI:
Registrazione alla piattaforma
Inserimento di un videogioco nella wishlist
Visualizzazione della propria wishlist
Noleggio/Prenotazione di un videogioco
Visualizzazione di storico e futuro dei propri noleggi
Visualizzazione dei dettagli di ciascun noleggio/prenotazione
Visualizzazione delle informazioni di fatturazione, in caso di noleggio concluso
Richiesta di modifica della data di inizio e della data di fine di una prenotazione
Richiesta di cancellazione di una prenotazione
Visualizzazione e modifica dei propri dati
Visualizzazione della propria carta fedeltà e del proprio saldo punti
Visualizzazione delle notifiche

OPERAZIONE RISERVATE AGLI AMMINISTRATORI:
Visualizzazione della lista degli utenti
Creazione di un account
Modifica dei dati di un utente
Blocco/Sblocco di un utente
Invio di una notifica ad un utente
Visualizzazione di storico e futuro di tutti i noleggi
Chiusura di un noleggio
Retrodatazione di un noleggio
Aggiunta di un nuovo videogioco
Cancellazione di un videogioco, se mai noleggiato
Modifica delle informazioni di un videogioco, compresa la sua disponibilità