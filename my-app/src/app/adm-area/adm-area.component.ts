import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-area',
  template: `
  <main>
    <div class="container d-flex flex-column justify-content-center pt-5"  id="adminMenu_container">
      <h1 class="text-center text-white mb-1">Bentonato nome</h1> 
      <p class="text-white text-center mb-5 mb-lg-0">Ecco il tuo menu principale</p>
      <div class="row row-cols-1 row-cols-lg-1 row-cols-md-1 mt-lg-5 first_row d-flex justify-content-center">
        <div class="col d-flex justify-content-center align-items-center">
          <div class="link_wrapper border  d-flex align-items-center">
            <a class="nav-link w-100" routerLink="/adm_area/amd_allOrders"><i class="bi bi-box-seam me-2"></i> Gestisci i noleggi <i class="bi bi-chevron-right float-end arrow_icon"></i></a>
          </div>
        </div>
        <div class="col d-flex justify-content-center align-items-center">
          <div class="link_wrapper border d-flex align-items-center">
          <a class="nav-link w-100" routerLink="/adm_area/adm_manageUsers"><i class="bi bi-person me-2"></i> Gestisci gli utenti <i class="bi bi-chevron-right float-end arrow_icon"></i></a>
          </div>
        </div>
        <div class="col d-flex justify-content-center align-items-center">
          <div class="link_wrapper border  d-flex align-items-center">
            <a class="nav-link w-100" routerLink="/adm_area/adm_manageVideogames"><i class="bi bi-emoji-heart-eyes me-2"></i> Gestisci i videogiochi <i class="bi bi-chevron-right float-end arrow_icon"></i></a>
          </div>
        </div>
      </div>
      <div class="row d-flex justify-content-center align-items-center mt-4 second_row">
        <span class="text-center"><a class="text-secondary">Logout</a></span>
      </div>
    </div>
  </main>
  `,
  styles: [
    `
    main{
      padding-top: 150px;
    }
    #adminMenu_container .first_row .link_wrapper{
      width:99%;
      height: 83%;
    }
    #adminMenu_container .first_row .col a{
      color:white;
      text-transform: uppercase;
    }
    #adminMenu_container .second_row{
      height: 5vh;
    }
    @media(min-width:576px){
      #adminMenu_container .first_row .col a{
        text-align:center;
        weight: 800;
      }
      #adminMenu_container .first_row .col a i{
        display:none;
      }
      #adminMenu_container .first_row .col{
        height: 10vh;
      }
      #adminMenu_container .first_row .link_wrapper{
        width: 60%;
      }
    }
    `
  ]
})
export class AdminAreaComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
