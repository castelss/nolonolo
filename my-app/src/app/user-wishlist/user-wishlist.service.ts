import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserWishlistService {
  private baseUrl: string = "http://localhost:4000/users/";

  constructor(private _httpClient: HttpClient) { };
  
  getWishList(){
    return this._httpClient.get(this.baseUrl+"getWishlist");
  }
}
