import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from '../login/login.service';
import { Videogame } from '../../../../backend/models/videogames';
import { UserWishlistService } from './user-wishlist.service';

@Component({
  selector: 'app-user-wishlist',
  template: `
  <main>
    <div class="container-fluid container-lg">
    <h3 class="text-white">La tua lista dei desideri</h3>
      <div class="row row-cols-1 row-col-sm-2 row-col-md-2 row-col-lg-3 mt-5">
        <div class="col-sm-4 mb-5 mt-lg-4 d-flex justify-content-center"*ngFor="let videogame of videogames" >
          <a class="text-decoration-none" routerLink="#" >
            <div class="card border-0 w-100">
              <img [src]="videogame.image" class="card-img-top" alt="videogame">
              <div class="card-body p-0 pt-1 d-flex justify-content-between">
                  <h6 class="card-title text-white ">{{videogame.nome}}</h6>
                  <button><i class="bi bi-heart-fill"></i></button>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  </main>
  `,
  styles: [
    `
  
    .card{
      background:#111111;
    }
    main{
      padding-top: 5vh;
    }
    button{
      border:none;
      color: #64737c;
      background:transparent;
    }
    
    `
  ]
})
export class UserWishlistComponent implements OnInit {
  public videogames : any = Videogame;
  
  constructor(private _userWishlistService : UserWishlistService,
    private activatedRoute: ActivatedRoute,
    private router: Router,) { }

  ngOnInit(): void {
    this._userWishlistService.getWishList().subscribe(videogames =>{
      this.videogames = videogames['videogame'];
      console.log(videogames)
    })
  }

}
