import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AdmManageOrdersService {
  private baseUrl: string = "http://localhost:4000/";

  constructor(private _httpClient: HttpClient) { };

  getRent(id){
   return this._httpClient.get(this.baseUrl+"admin/noleggi/"+id);
  }

  updateFattura(form){
    return this._httpClient.post(this.baseUrl+"admin/fattura", form);
  }

  updateRent(form){
    return this._httpClient.post(this.baseUrl+"admin/modifica", form);
  }

}
