import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdmManageOrdersService } from './adm-manage-orders.service';
import { Rent } from '../../../../backend/models/rent'
import { FormBuilder, FormGroup } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-adm-manage-orders',
  template: `
  <main>
    <div class="container-fluid container-lg d-flex flex-column justify-content-center pt-5 mb-4">
      <h3 class="text-white mb-1">Segui le istruzioni per chiudere questo noleggio</h3> 
      <div class="wrapper mt-4">
        <ol class="list-group list-group-numbered">
          <li class="list-group-item d-flex justify-content-between align-items-start">
            <div class="ms-2 me-auto">
              <div class="fw-bold">Controlla lo stato del noleggio</div>
              <span>Lo stato di questo noleggio è {{rent.stato}} .</span>
              <span *ngIf="rent.stato==='attivo'"> Perfetto! Questo significa che puoi continuare la procedura.</span> <!--SE IL NOLEGGIO è CONCLUSO-->
              <span *ngIf="rent.stato!='attivo'"> Purtroppo non è possibile continuare la procedura, il noleggio è ancora in corso.</span> <!--SE IL NOLEGGIO E' ATTIVO-->
            </div>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-start">
            <div class="ms-2 me-auto">
              <div class="fw-bold">Controlla se il cliente {{rent.user.nome}} {{rent.user.cognome}} ha diritto ad uno sconto</div>
              <span>Il saldo punti del cliente è : {{rent.user.punti}}</span>
            </div>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-start">
            <div class="ms-2 me-auto form_wrapper">
              <div class="fw-bold">Calcolare l'importo dovuto e chiudere il noleggio</div>
              <form method="post" class="w-100 pe-2" (ngSubmit)="updateFattura()" [formGroup]="formFattura">
              <label>Identificativo del noleggio</label>
              <p class="form-control mb-2 border rounded">{{rent._id}}</p>
              <label>Metodo di pagamento</label>
              <select class="form-select form-select-sm mb-3" aria-label=".form-select-lg" name="metodo" formControlName="metodo">
                <option value="1">Carta</option>
                <option value="2">Contanti</option>
              </select>
              <label>Sconto sul totale</label>
              <select class="form-select form-select-sm mb-2" aria-label=".form-select-lg" name="sconto" formControlName="sconto">
                <option value="1">€ 0.00</option>
                <option value="2">€ {{rent.user.sconto}},00</option>
              </select>
              <label>Giorni di ritardo alla riconsegna</label>
              <select class="form-select form-select-sm mb-2" aria-label=".form-select-lg" name="mora" formControlName="mora">
                <option value="1">0</option>
                <option value="2">1-5</option>
                <option value="3">5-10</option>
                <option value="4">10-20</option>
                <option value="5">20-30</option>
                <option value="6">30+</option>
              </select>
              <label>Aggiungi una nota</label>
              <textarea class="form-control mb-2" rows="1" name="nota" formControlName="nota"></textarea>
              <label>Stato del videogioco alla riconsegna</label>
              <select class="form-select form-select-sm mb-2" aria-label=".form-select-lg" name="stato" formControlName="stato"> 
                <option value="1">Buono</option>
                <option value="2">Scarso</option>
                <option value="3">Ottimo</option>
              </select>
              <button type="submit" name='submit' class="btn text-white w-100" *ngIf="rent.stato==='attivo'"> CHIUDI NOLEGGIO</button>
              <button type="submit" name='submit' class="btn text-white w-100" *ngIf="rent.stato!='attivo'" disabled> CHIUDI NOLEGGIO</button>
            </form>
            </div>
          </li>
        </ol>
      </div>
      <h3 class="text-white mb-1 mt-5">Segui le istruzioni per retrodatare il noleggio</h3>
      <div class="wrapper mt-4">
        <ol class="list-group list-group-numbered">
          <li class="list-group-item d-flex justify-content-between align-items-start">
            <div class="ms-2 me-auto form_wrapper">
              <div class="fw-bold">Compila il seguente modulo</div>
              <form method="post" class="w-100" (ngSubmit)="retrodata()" [formGroup]="retrodataForm">
                <label class=" py-2 text-dark"> La data di fine noleggio attualmente è : {{rent.dataFine | date}}</label>
                <br>
              <label>Puoi selezionare una nuova data di fine noleggio</label>
              <input type="date" class="form-control mb-3" formControlName="dataFine">
              <button type="submit" name='submit' class="btn text-white w-100 gradient_background"> RETRODATA  </button>
            </form>
            </div>
          </li>
        </ol>
      </div>
    </div>
  </main>
  `,
  styles: [
    `

    main{
      padding-top: 15vh;
    }
    button{
      background:#64737c;
    }
    .form_wrapper{
      width:100%;
    }
    @media(min-width:480px){
      .form_wrapper{
        width:50%;
      }
    }
    
    `
  ]
})
export class AdmManageOrdersComponent implements OnInit {
  id: any
  rent: Rent = {}
  formFattura: FormGroup
  retrodataForm : FormGroup

  constructor(private activatedRoute : ActivatedRoute, private _admManageOrderService : AdmManageOrdersService,
    private formBuilder : FormBuilder, private router : Router) {
      this.rent.user = {};
      this.rent.videogioco ={};
      this.formFattura = this.formBuilder.group({id: this.id ,metodo: "", sconto:"", mora:"", nota: "", stato: ""})
      this.retrodataForm = this.formBuilder.group({id:"", dataInizio:"", dataFine:""})
     }

  

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.params['id']
      if(this.id){
        this._admManageOrderService.getRent(this.id).subscribe(
          res => {
            this.rent = res['rent'];
          },
          err => { 
            alert("dettagli non disponibili");
            this.router.navigateByUrl('/adm_area')
          }
        )
      }
    
  }

  updateFattura(){
    this.formFattura.value['id']=this.id;
    this._admManageOrderService.updateFattura(this.formFattura.value).subscribe(
      res => {
        this.formFattura.reset()
        Swal.fire('OK :)', 'Pagamento Effettuato', 'success')
        this.router.navigate(['/customer_rentals']);
      },
      err => {
        this.formFattura.reset()
        Swal.fire('OOPS :(', 'Qualcosa è andato storto', 'error')
        this.router.navigate(['/customer_rentals']);
      }
    )
  }

  retrodata(){
    this.retrodataForm.value['id']=this.id;
    this.retrodataForm.value['dataInizio']=this.rent.dataInizio;
    this._admManageOrderService.updateRent(this.retrodataForm.value).subscribe(
      res => {
        this.retrodataForm.reset()
        Swal.fire('OK :)', 'Noleggio Retrodatato', 'success')
        this.router.navigate(['/customer_rentals']);
      },
      err => {
        this.retrodataForm.reset()
        Swal.fire('OOPS :(', 'Prova a cambiare la data', 'error')
        this.router.navigate(['/customer_rentals']);
      }
    )
  }

}