import { Component, OnInit } from '@angular/core';
import { RegistartionService } from './registartion.service';
import { Router } from "@angular/router";
import { FormBuilder, NgForm } from "@angular/forms";
import Swal from 'sweetalert2/dist/sweetalert2.js';


@Component({
  selector: 'app-registration',
  template: `
  <main>
    <div class="container-fluid container-lg">
      <div class="row d-flex justify-content-start align-items-center first_row">
        <div class="col-sm-12 col-md-12 col-lg-8 d-flex align-items-center justify-content-start rounded p-sm-5">
          <form #signUpForm="ngForm" (ngSubmit)="onSubmit(signUpForm)" method="post" class="px-3">
            <h2 class="text-white mb-1">Inizia con noi una nuova avventura</h2>
            <div class="row row-cols-1 row-cols-sm-3">
              <div class="col form-floating">
                <input type="text" placeholder="Inserisci il tuo nome" class="form-control border-bottom border-secondary" name="nome" required #nome="ngModel"  [(ngModel)]="_regService.selectedUser.nome">
                <label class="text-white">Inserisci il tuo nome</label>
              </div>
              <div class="col form-floating">
                  <input type="text" placeholder="Inserisci il tuo cognome" class="form-control border-bottom border-secondary" name="cognome" required #cognome="ngModel"  [(ngModel)]="_regService.selectedUser.cognome">
                  <label class="text-white">Inserisci il tuo cognome</label>
              </div>
              <div class="col form-floating">
                  <input type="text" placeholder="Inserisci il tuo username" class="form-control border-bottom border-secondary" name="username" required #username="ngModel"  [(ngModel)]="_regService.selectedUser.username">
                  <label class="text-white">Inserisci lo username</label>
              </div>
              <div class="col form-floating">
                  <input type="email" placeholder="Inserisci il tuo indirizzo E-mail" class="form-control border-bottom border-secondary" name="email" required #email="ngModel"  [(ngModel)]="_regService.selectedUser.email">
                  <label class="text-white">Inserisci la tua e-mail</label>
              </div>
              <div class="col form-floating">
                  <input type="password" placeholder="Inserisci la tua password" class="form-control border-bottom border-secondary" name="password" minlength="6"  required #password="ngModel"  [(ngModel)]="_regService.selectedUser.password">
                  <label class="text-white">Inserisci la tua password</label>
              </div>
              <div class="col form-floating">
                  <input type="date" placeholder="Inserisci la tua data di nascita" class="form-control border-bottom border-secondary" name="dataNascita" >
                  <label class="text-white">Inserisci la tua data di nascita</label>
              </div>
            </div>
            <button type="submit" class="btn text-white w-100 rounded mt-5 text-uppercase"> Registrati </button>
            <p class="text-secondary text-center mt-5">Hai già un'account? Clicca <a routerLink="/login" class="text-secondary text-decoration-underline">QUI</a> per accedere.</p>
          </form>
        </div>
      </div>
    </div>
  </main>
  `,
  styles: [`
    .first_row{
      height:85vh;
    }
    input{
      border:none;
      border-radius:0px;
      background:#111111;
    }
    input.form-control{
      color:white;
    }
    input ::placeholder{
      color:white;
    }
    input:focus{
      outline: none !important;
      box-shadow: 0 0 0 transparent;
      background:#111111;
    }
    
    button{
      background:#64737c;
    }

    .alert_message p{
      background:#64737c;
    }
    ::-webkit-calendar-picker-indicator {
      filter: invert(1);
    }
  `]
})



export class RegistrationComponent implements OnInit {

  error : any = null

  constructor(private router : Router,
    public _regService: RegistartionService,
    private formBuilder: FormBuilder) {}

  ngOnInit(): void {
  }

  onSubmit(form: NgForm) {
    console.log(form.value);
    this._regService.register(form.value).subscribe(
      res => {
        form.reset()
        Swal.fire('Benvenuto', 'La registrazione è andata a buon fine, inserisci le tue credenziali per accedere', 'success')
        this.router.navigate(['/login']);
      },
      err => {
        form.reset()
        this.error = err
        Swal.fire('OOPS :(', err.error.message, 'error')
      }
    );
  }
  
  resetForm(form: NgForm) {
    this._regService.selectedUser = {
      nome: '',
      cognome: '',
      username:'',
      email: '',
      password: ''
    };
    form.resetForm();
  }

}
