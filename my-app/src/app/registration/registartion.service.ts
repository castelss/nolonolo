import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../../../backend/models/user.js';

@Injectable({
  providedIn: 'root'
})
export class RegistartionService {

  selectedUser: User = {
    nome: '',
    cognome: '',
    username:'',
    email: '',
    password: '',
    role: 'user'
  };

  private baseUrl: string = "http://localhost:4000/users/registration/";

  constructor(private _httpClient: HttpClient) { };

  register(user: User){
    console.log(this.baseUrl, user)
    return this._httpClient.post(this.baseUrl, user)

  } 

}
