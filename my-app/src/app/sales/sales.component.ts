import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sales',
  template: `
  <main>
  <div class="container-fluid container-lg">
  <h3 class="text-white mt-5">Il nostri saldi</h3>
    <div class="row row-cols-1 row-col-sm-2 row-col-md-2 row-col-lg-3 mt-3">
      <div class="col-sm-4 mb-5 mt-lg-4 d-flex justify-content-center" >
        <a  class="text-decoration-none">
          <div class="card border-0 w-100">
            <img src="https://media.istockphoto.com/photos/canvas-art-background-white-total-linen-texture-cotton-pattern-picture-id1277594462?b=1&k=20&m=1277594462&s=170667a&w=0&h=qTvOztHgihy2sffsxYbEDtLnCUcJomMeLd9SWRFKik8=" class="card-img-top" alt="videogame">
            <div class="card-body ps-0">
                  <h6 class="card-title text-white">From other suns</h6>
                  <p class="text-secondary"><span class="px-1 percentage rounded me-2"> -20% </span>30,00€<span class="text-decoration-line-through ms-2">50,00€</span></p>
                </div>
          </div>
        </a>
      </div>
    </div>
  </div>
</main>
`,
styles: [`

  .container-fluid{
    padding-top: 5vh;
  }
  .card{
    background:#111111;
  }
  .percentage{
    background: #64737c;
    color: white;
  }
`]
})
export class SalesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
