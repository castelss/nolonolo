import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserInfoService {

  getUserProfile() {
    return this._httpClient.get(this.baseUrl);
  }

  updateUser(form : any){
    return this._httpClient.post(this.baseUrl+"update",form);
  }

  updatePw(form : any){
    return this._httpClient.post(this.baseUrl+"updatePw",form);
  }

  getMsg(){
    return this._httpClient.get(this.baseUrl+"msg");
  }

  private baseUrl: string = "http://localhost:4000/users/";

  constructor(private _httpClient: HttpClient) { }

}
