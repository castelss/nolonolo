import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from '../login/login.service';
import { UserInfoService } from './user-info.service';
import { User } from '../../../../backend/models/user';

@Component({
  selector: 'app-user-info',
  template: `
  <main>
    <div class="container-fluid container-lg d-flex flex-column justify-content-center align-items-start">
      <div class="row row-cols-1 w-100 mt-5">
        <div class="col-sm-12 h-100">
          <h3 class="text-white">La tua fidelity card</h3>
          <div class="row row-cols-1 row-cols-sm-2">
            <div class="col">
              <div class="mt-3 mb-3 fidelity_card shadow-sm rounded d-flex flex-column justify-content-center align-items-start px-4 py-5 ">
                <p class="text-white fw-bold">N° {{user._id}}</p>
                <div class="w-100 d-flex justify-content-start">
                  <p class="text-white mb-1 fw-bold me-2">Intestatario</p>
                  <p class="text-white mb-0">{{user.nome}} {{user.cognome}}</p>
                </div>
              </div>
            </div>
            <div class="col d-flex justify-content-start align-items-center">
              <div class="p-2">
                <h6 class="text-white">Il tuo saldo punti</h6>
                <p class="points mt-3 text-white"><span><b>{{user.punti | number : '1.0-0'}} Punti</b></span> | <span> {{user.sconto}},00 €  </span></p>
                <button type="button" class="btn rounded text-white" data-bs-toggle="modal" data-bs-target="#modal_4pointsInfo">
                  Scopri le soglie punti<i class="bi bi-info-circle ms-3"></i>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-12 mt-5 mb-5">
          <h3 class="text-white mb-3 px-2">I tuoi dati personali</h3>
          <p class="text-secondary px-2">Visualizza e modifica i dati che ci hai fornito al momento della registrazione. Attenzione, l'email e la data di nascita inserita al momento della registrazione non possono essere modificate.</p>
          <form class="px-2 mb-3 mt-5" (ngSubmit)="updateUser()" [formGroup]="updateUserForm">
            <div class="mb-3">
              <label class="form-label text-white">Il tuo nome</label>
              <input type="email" class="form-control" placeholder="{{user.nome}}" formControlName="nome"  name=nome>
            </div>
            <div class="mb-3">
              <label class="form-label text-white">Il tuo cognome</label>
              <input type="email" class="form-control" placeholder="{{user.cognome}}" formControlName="cognome"  name=cognome>
            </div>
            <div class="mb-3">
              <label class="form-label text-white">Il tuo username</label>
              <input type="email" class="form-control" placeholder="{{user.username}}"formControlName="username"  name=username>
            </div>
            <div class="mb-3">
              <label class="form-label text-white">La tua data di nascita</label>
              <input type="date" class="form-control" value="{{user.dataNascita | date}}" formControlName="dataNascita"  name=dataNascita>
            </div>
            <div class="mb-3">
              <label class="text-white text-white">La tua e-mail : {{user.email}}</label>
            </div>
            <button type="submit" class="btn w-100   rounded-pill text-white mt-3">Salva le modifiche</button>
          </form>
          <p class="text-secondary mt-5">Se invece vuoi cambiare password:</p>
            <form (ngSubmit)="updatePw()" [formGroup]="updatePwForm">
              <div class="form-floating mb-3">
                <input type="password" class="form-control" placeholder="pw" formControlName="old"  name=old>
                <label class="text-white">Inserisci la tua attuale password</label>
              </div>
              <div class="form-floating mb-3">
                <input type="password" class="form-control" placeholder="Password" formControlName="new"  name=new>
                <label class="text-white">Inserisci la tua nuova password</label>
              </div>
              <button type="submit" class="btn w-100  rounded-pill mt-5 text-white">Cambia password</button>
            </form>
          </div>
        </div>
      </div>

      <!--MODAL FOR POINTS THRESHOLD INFO-->

      <div class="modal fade" id="modal_4pointsInfo" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="btn-close btn-light" data-bs-dismiss="modal" aria-label="Close"><i class="bi bi-x-lg"></i></button>
            </div>
            <div class="modal-body">
              <ul>
                <li> 200 punti = 5 euro</li>
                <li> 400 punti = 12 euro</li>
                <li> 600 punti = 18 euro</li>
                <li> 800 punti = 24 euro</li>
                <li> 1000 punti = 30 euro</li>
              </ul>
            </div>
          </div>
        </div>
    </div>
  </main>
  `,
  styles: [
    `
    input{
      border:none;
      border-radius:0px;
      border-bottom: 1px solid white;
      background: #111111;
    }
    
    input:focus{
      outline: none !important;
      box-shadow: 0 0 0 transparent;
      border-bottom: 1px solid white;
      background: #111111;
    }
    input.form-control{
      color:white;
    }
    ::-webkit-calendar-picker-indicator {
      filter: invert(1);
  }

    .modal input{
      background: white;
      border-bottom: 1px solid black;
    }
    .modal input:focus{
      border-bottom: 1px solid black;
      background: white;
    }
    .modal button{
      background:white;
      color:black;
    }
    .fidelity_card{
      width:100%;
      background:#64737c;
    }
    button{
      background:#64737c;
    }

    button:focus{
      outline: none !important;
      box-shadow: 0 0 0 transparent;
    }
    @media(min-width:576px){
      .fidelity_card{
        width:60%;
      }
      form{
        width: 40%;
      }
    }
    `
  ]
})
export class UserInfoComponent implements OnInit {

  user: User = {
    _id: '',
    nome: '',
    cognome: '',
    username: '',
    dataNascita: Date.now, 
    createdAt: ''
  }

  updateUserForm : FormGroup
  updatePwForm : FormGroup

  constructor(private _loginService: LoginService,private router : Router, private _userInfoService: UserInfoService,
    private activatedRoute : ActivatedRoute, private formBuilder: FormBuilder) {
      this.updateUserForm = this.formBuilder.group({nome: "", cognome:"", username:"", email:"", dataNascita: "" });
      this.updatePwForm = this.formBuilder.group({old: "", new: ""});
     }

  ngOnInit(): void {
    this._userInfoService.getUserProfile().subscribe(
      res => {
        //console.log(res['user'])
        this.user = res['user'];
      },
      err => { 
        console.log(err);
      }
    );
  }

  updateUser(){
    this._userInfoService.updateUser(this.updateUserForm.value).subscribe(
      res => {
        this.updateUserForm.reset()
        this.router.navigate(['/catalog']);
      },
      err => {
        this.updateUserForm.reset()
        alert(err)
      }
    )
  }

  updatePw(){
    this._userInfoService.updatePw(this.updatePwForm.value).subscribe(
      res => {
        this.updatePwForm.reset()
        this.router.navigate(['/catalog']);
      },
      err => {
        this.updatePwForm.reset()
        alert(err)
      }
    )
  }

}
