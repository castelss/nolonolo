import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-adm-all-users',
  template: `
  <main class="container">
    <h1 class="text-white text-center">La lista dei clienti</h1>
    <div class="row row-cols-1 border text-white mt-5">
      <input class="text-black" type="search" placeholder="Filtra il contenuto.." aria-label="Search">
      <div class="col d-flex justify-content-between border-bottom">
        <p>Nome cliente</p>
        <p>8 noleggi</p>
      </div> 
      <div class="col d-flex justify-content-between border-bottom">
        <p>Nome cliente</p>
        <p>8 noleggi</p>
      </div> 
      <div class="col d-flex justify-content-between border-bottom">
        <p>Nome cliente</p>
        <p>8 noleggi</p>
      </div> 
    </div>
  </main>  
  `,
  styles: [
    `
    main{
      padding-top: 150px;
    }
    .col{
      height: 5vh;
    }
    p{
      line-height: 5vh;
    }
    input{
      height: 8vh;
    }
    `
  ]
})
export class AdmAllUsersComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
