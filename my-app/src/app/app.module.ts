import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { CatalogComponent } from './catalog/catalog.component';
import { VideogameComponent } from './videogame/videogame.component';
import { UserWishlistComponent } from './user-wishlist/user-wishlist.component';
import { UserOrdersComponent } from './user-orders/user-orders.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { AdmManageOrdersComponent } from './adm-manage-orders/adm-manage-orders.component';
import { AdmManageUsersComponent } from './adm-manage-users/adm-manage-users.component';
import { AdmManageVideogamesComponent } from './adm-manage-videogames/adm-manage-videogames.component';
import { AdmAllOrdersComponent } from './adm-all-orders/adm-all-orders.component';
import { LoginService } from './login/login.service';
import { AuthGuard } from './auth/auth.guard';
import { AuthInterceptor } from './auth/auth.interceptor';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { UserInfoComponent } from './user-info/user-info.component';
import { UserNotificationsComponent } from './user-notifications/user-notifications.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    CatalogComponent,
    VideogameComponent,
    UserWishlistComponent,
    UserOrdersComponent,
    OrderDetailsComponent,
    LoginComponent,
    RegistrationComponent,
    AdmManageOrdersComponent,
    AdmManageUsersComponent,
    AdmManageVideogamesComponent,
    AdmAllOrdersComponent,
    UserInfoComponent,
    UserNotificationsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  },AuthGuard,LoginService],
  bootstrap: [AppComponent]
})
export class AppModule { }
