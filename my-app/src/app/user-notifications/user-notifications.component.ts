import { Component, OnInit } from '@angular/core';
import { UserInfoService } from '../user-info/user-info.service';

@Component({
  selector: 'app-user-notifications',
  template: `
  <main>
  <div class="container-fluid container-lg d-flex flex-column justify-content-center align-items-start">
    <div class="row row-cols-1 w-100 mt-5">
      
      <div class="col-sm-12 h-100">
      <h3 class="text-white mb-5">Le tue notifiche</h3>
       <div *ngFor="let msg of messages"  class="wrapper border-bottom border-secondary d-flex justify-content-between align-items-center">
       <p class="text-white">{{msg.testo}}</p>
       <p class="text-secondary"></p>
       </div>
      </div>
    </div>
  </div>

</main>
  `,
  styles: [
    `
    
    `
  ]
})
export class UserNotificationsComponent implements OnInit {

  constructor(private _userInfoService :UserInfoService) { }
  messages : any = []
  ngOnInit(): void {
    this._userInfoService.getMsg().subscribe(messages =>{
      this.messages = messages;
    })
  }

}
