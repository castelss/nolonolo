import { Component, OnInit } from '@angular/core';
import { CatalogService } from '../catalog/catalog.service';
import {Videogame} from '../../../../backend/models/videogames'
declare var countdown: any;

@Component({
  selector: 'app-home',
  template: `
    <main>
      <div class="container-fluid container-lg">
        <div class="row row-cols-1 row-cols-sm-2 row-cols- py-lg-3 px-2 px-sm-3 mb-3">
          <div class="col p-0 py-lg-3 d-flex justify-content-start align-items-center">
            <div class="wrapper ps-4 ps-sm-2 w-100 text-white">
              <h1 class="text-uppercase">Sperimenta l'inimmaginabile</h1>
              <p class="mb-4">Vivi una nuova avventura in realtà virtuale</p>
              <a class="text-uppercase border rounded-pill px-3 py-2 text-decoration-none text-white" href="#scroll">Inizia ora</a>
            </div>
          </div>
          <div class="col p-0 py-lg-3 d-flex justify-content-start align-items-center">
            <img src="assets/img/boy_visor.png" alt="boy_with_visor" class="img-fluid">
          </div>
        </div>
        <div class="row row-cols-1 row-cols-sm-2 row-cols-lg-3 px-3 py-5">
          <div class="col-sm-6 col-lg-4">
            <div class="wrapper h-100 d-lg-flex flex-lg-column align-items-lg-start">
              <h1 class="text-md-uppercase text-white">Insieme è tutto <br class="d-none d-sm-block"> più divertente</h1>
              <p class="text-grey mb-4">Ovunque siate e qualsiasi cosa vogliate fare, noi siamo pronti ad offrire a te e ai tuoi amici una nuova avventura da vivere insieme. Se stai cercando giochi multiplayer a realtà virtuale, sei nel posto giusto! <br>  La proposta di oggi è: <a routerLink="/catalog/videogame/56575757" class="text-white text-decoration-none">GREYMOOR</a>.</p>
              <p class="or mb-4">oppure</p>
              <a routerLink="/catalog" class="text-decoration-none text-white px-2 py-1 border rounded-pill">Scopri tutto il catalogo</a>
            </div>
          </div>
          <div class="col-sm-6 col-lg-4 mt-5 mt-sm-0">
           <img src="assets/img/greymoor_cover.jpg" alt="greymoor_cover" class="img-fluid h-100">
          </div>
          <div class="col-lg-4 mt-5 mt-sm-0 d-none d-lg-block">
            <img src="assets/img/greymoor_set.jpg" alt="greymoor_set" class="img-fluid h-100">
          </div>
        </div>
        <h1 class="text-white ms-3">Solo a febbraio</h1>
        <div class="row row-cols-1 row-cols-sm-2 row-cols-lg-2 px-3 px-lg-5 py-5" style="background: #64737c;" id="scroll"> 
          <div class="col-sm-9 col-md-8 ">
            <div class="wrapper h-100 ms-sm-3 ms-md-0 position-relative">
              <p class="text-white mt-3 mt-sm-0 mb-md-5">Conosci il nostro sistema di punti bonus? Più spendi, più saranno i punti accumulati sulla fidelity card, maggiore sarà lo sconto per il tuo prossimo acquisto! Se ti iscrivi al nostro servizio entro la fine di Febbraio, ti facciamo un regalo: 100 punti bonus, per la simpatia!</p>
              <div class="wrapper w-100 d-none d-md-block">
                <span class="text-white mb-3 mb-md-0 position-absolute bottom-0 start-0">Questa iniziativa terminerà tra <span id="countdown"></span></span>
              </div>
            </div>
          </div>
          <div class="col-sm-3 col-md-4 d-sm-flex justify-content-sm-end align-items-sm-end">
            <a routerLink="/registration" class="text-decoration-none text-white">Ti abbiamo convinto? <i class="bi bi-arrow-right"></i></a>
          </div>
        </div>
        <div class="row px-3 py-5 d-flex justify-content-center">
          <div class="wrapper">
            <div class="wrapper d-flex justify-content-between align-items-center">
              <h3 class="text-white">TOP 3 NOLEGGI</h3>
              <a routerLink="/sales" class="text-decoration-none text-white px-1 py-0 px-md-2 py-md-0 border rounded-pill d-block d-sm-none d-lg-block text-center">Vedi tutto</a>
            </div>
            <p class="text-grey mt-2 mt-md-0">Qui abbiamo sempre un buon motivo per offrirvi del divertimento a prezzi favolosi.</p>
          </div>
          <div class="row p-0 row-cols-1 row-cols-sm-2 row-cols-md-2 row-cols-lg-3 mt-4 mt-md-0">
            <div class="col mt-2">
              <div class="card border-0 w-100">
                <img [src]="videogames[0].image" class="card-img-top" alt="videogame_cover">
                <div class="card-body ps-0">
                  <h6 class="card-title text-white">{{videogames[0].nome}}</h6>
                  <p class="text-grey">{{videogames[0].pzFissoBaby | number : '1.2-2'}} €</p>
                </div>
              </div>
            </div>
            <div class="col mt-2">
              <div class="card border-0 w-100">
                <img [src]="videogames[1].image" class="card-img-top" alt="videogame_cover">
                <div class="card-body ps-0">
                  <h6 class="card-title text-white">{{videogames[1].nome}}</h6>
                  <p class="text-grey">{{videogames[1].pzFissoBaby | number : '1.2-2'}} €</p>
                </div>
              </div>
            </div>
            <div class="col mt-2">
              <div class="card border-0 w-100">
                <img [src]="videogames[2].image" class="card-img-top" alt="videogame_cover">
                <div class="card-body ps-0">
                  <h6 class="card-title text-white">{{videogames[2].nome}}</h6>
                  <p class="text-grey"> {{videogames[2].pzFissoBaby | number : '1.2-2'}} €</p>
                </div>
              </div>
            </div>
            <div class="col d-none d-sm-block d-lg-none d-sm-flex justify-content-sm-center align-items-sm-center">
              <a href="" class="text-decoration-none text-white border rounded-pill px-3 py-2 text-uppercase">Vedi tutto <i class="bi bi-arrow-right ms-3"></i></a>
            </div>
          </div>
        </div>
        <div class="row row-cols-1 row-cols-sm-2 row-cols-lg-2 px-3 py-4 mb-3"> 
          <div class="col-sm-3 col-lg-4 d-flex justify-content-start align-items-start">
            <h1 class="text-white fst-italic">We 
              <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-chat-square-heart ms-1" viewBox="0 0 16 16">
                <path d="M14 1a1 1 0 0 1 1 1v8a1 1 0 0 1-1 1h-2.5a2 2 0 0 0-1.6.8L8 14.333 6.1 11.8a2 2 0 0 0-1.6-.8H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12ZM2 0a2 2 0 0 0-2 2v8a2 2 0 0 0 2 2h2.5a1 1 0 0 1 .8.4l1.9 2.533a1 1 0 0 0 1.6 0l1.9-2.533a1 1 0 0 1 .8-.4H14a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2Z"/>
                <path d="M8 3.993c1.664-1.711 5.825 1.283 0 5.132-5.825-3.85-1.664-6.843 0-5.132Z"/>
              </svg> <br class="d-none d-sm-block"> 
              nolonolo
            </h1>
          </div>
          <div class="col-sm-9 col-lg-8 ">
            <div class="wrapper h-100 ms-sm-3 ms-md-0 position-relative">
              <p class="text-grey mt-3 mt-sm-0">Ci piace sapere come ti diverti con i videogiochi noleggiati da noi! Condividi un post sul tuo social preferito che rappresenti la tua esperienza con il nostro servizio, tagga @nolonolo oppure utilizza l'hashtag #nololover. Vi ripostiamo tutti.</p>
              <p class="text-grey mb-5">E se vuoi essere sempre aggiornato sugli ultimi arrivi, segui le nostre pagine social!</p>
              <ul class="d-flex m-0 p-0 me-3 mb-3 position-absolute bottom-0 end-0">
                <li class="me-4"><a href="#"><i class="bi bi-facebook"></i></a></li>
                <li class="me-4"><a href="#"><i class="bi bi-instagram"></i></a></li>
                <li><a href="#"><i class="bi bi-twitter"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </main>
  `,
styles: [`
  .row:first-of-type{
    height: 80vh;
  }
  .row:first-of-type h1{
    font-size: 30px;
  }
  .text-grey{
    color: #e3ece9;
  }
  .or{
    font-size: 10px;
    color: rgb(201, 201, 201);
  }
  .row:nth-of-type(2) h1{
    width: 100%;
  }
  .row:nth-of-type(3) h1, .row:nth-of-type(5) h1{
    font-size: 5vw;
  }
  .row:nth-of-type(4) .row .card-body{
    background: #111111;
  }
  .row:nth-of-type(4) .row .card{
    background: #111111;
  }
  .percentage{
    background: #64737c;
    color: white;
  }
  .row:nth-of-type(5) ul li{
    list-style: none;

  }
  .row:nth-of-type(5) ul li a{
    color: white;
  }

  @media(min-width:540px){
    .row:first-of-type h1{
      font-size: 50px;
    }
    .row:nth-of-type(3) span{
        font-size: 1.1vw;
    }
  }
  
`]
})
export class HomeComponent implements OnInit {
  constructor(private _catalogService: CatalogService) { }
  videogames : Videogame = [];
  ngOnInit(): void {
    new countdown();
    this._catalogService.getTopTre().subscribe(videogames =>{
      this.videogames = videogames;
    })
  }

}
