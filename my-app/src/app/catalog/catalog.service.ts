import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ObjectId } from 'mongoose';

@Injectable({
  providedIn: 'root'
})
export class CatalogService {

  private baseUrl: string = "http://localhost:4000/videogames/";

  constructor(private _httpClient: HttpClient) { };

  getVideoGames(){
    return this._httpClient.get(this.baseUrl);
  }

  getTopTre(){
    return this._httpClient.get(this.baseUrl+"top3");
  }

}
