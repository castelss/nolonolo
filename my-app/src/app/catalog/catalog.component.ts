import { Component, OnInit } from '@angular/core';
import { CatalogService } from './catalog.service';


@Component({
  selector: 'app-catalog',
  template: `
  <main>
    <div class="container-fluid container-lg">
    <h3 class="text-white mt-5">Il nostro catalogo</h3>
    <div class="form-group w-100 mt-4 mb-5">
        <input type="text" class="form-control border-bottom border-bottom" placeholder="Filtra gli ordini.." [(ngModel)]="filterTerm">
      </div>
      <div class="row row-cols-1 row-col-sm-2 row-col-md-2 row-col-lg-3 mt-3">
        <div *ngFor="let videogame of videogames | filter:filterTerm" class="col-sm-4 mb-5 mt-lg-4 d-flex justify-content-center" >
          <a routerLink="/catalog/videogame/{{videogame._id}}" class="text-decoration-none">
            <div class="card border-0 w-100">
              <img class="card-img-top" [src]="videogame.image" [alt]="videogame.nome">
              <div class="card-body p-0 pt-1">
                  <h6 class="card-title text-white ">{{videogame.nome}}</h6>
              </div>
            </div>
          </a>
        </div>
      </div>
      <p *ngIf="!(videogames | filter:filterTerm).length" class="text-white">OOPS :( <br> Sembra che oggi non riusciremo ad accontentarti. Prova a cercare qualcos'altro.</p>
    </div>
  </main>
  `,
  styles: [`

    input{
      outline:none !important;
      border:none;
      background:#111111;
      border-radius:0px;
    }
    ::placeholder {
      color: white;
    }
    input.form-control{
      color:white;
      font-size:12px;
    }
    .form-group{
      width:80%;
    }
    input:focus{
      outline:none !important;
      box-shadow: 0 0 0 transparent;
      background:#111111;
    }
    .container-fluid{
      padding-top: 5vh;
    }
    .card{
      background:#111111;
    }
    @media(min-width:540px){
      .form-group{
        width:50%;
      }
    }
  `]
})
export class CatalogComponent implements OnInit {
  filterTerm;
  
  constructor(private _catalogService: CatalogService) { }

  public videogames : any = [];

  ngOnInit(): void {
    this._catalogService.getVideoGames().subscribe(videogames =>{
      this.videogames = videogames;
    })
    
  }

}
