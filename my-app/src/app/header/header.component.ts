import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login/login.service';
import { User } from '../../../../backend/models/user';
import { Router, ActivatedRoute } from "@angular/router";
import { UserInfoService } from '../user-info/user-info.service';

@Component({
  selector: 'app-header',
  template: `
    <header>
      <nav class="navbar navbar-expand-sm fixed-top">
        <div class="container-fluid container-lg">
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <a class="navbar-brand text-white me-sm-5" routerLink="/">nolonolo</a>
          <div class="collapse navbar-collapse py-5 py-sm-0 px-4 px-md-0 w-100 h-100 top-0" id="navbarCollapse">
            <a class="closebtn text-white text-decoration-none position-absolute top-0 d-block d-sm-none" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">&times;</a>
            <ul class="navbar-nav first_wrap me-auto mb-2 mb-sm-0">
              <li class="nav-item">
                <a class="nav-link" routerLink="/">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" routerLink="/catalog">Catalogo</a>
              </li>
            </ul>
            <ul class="navbar-nav mt-5 mt-sm-0 mb-2 mb-md-0">
              <li class="nav-item" *ngIf="!logged">
                <a routerLink="/login" class="nav-link">Login</a>
              </li>
              <!--SE LOGGATO, DROPDOWN PER MODALITA MOBILE (XS) -->
              <li *ngIf="logged" class="nav-item dropdown d-block d-sm-none">
                <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Area Personale</a>
                <!-- se cliente -->
                <ul *ngIf="user.ruolo !== 'admin'" class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li><a class="text-white text-decoration-none mb-3 fw-bold dropdown-item" routerLink="/my_info"><i class="bi bi-gear"></i>Il tuo profilo</a></li>
                  <li><a class="text-white text-decoration-none mb-3 fw-bold dropdown-item" routerLink="/my_notifications"><i class="bi bi-bell"></i>Le tue notifiche</a></li>
                  <li><a class="text-white text-decoration-none mb-3 fw-bold dropdown-item" routerLink="/my_rentals"> <i class="bi bi-cart"></i>I tuoi noleggi</a></li>
                  <li><a class="text-white text-decoration-none mb-3 fw-bold dropdown-item" routerLink="/my_wishlist"><i class="bi bi-heart"></i>La tua wishlist</a></li>
                </ul>
                <!-- se amministratore -->
                <ul *ngIf="user.ruolo === 'admin'" class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li><a class="text-white text-decoration-none mb-3 fw-bold dropdown-item" routerLink="/customer_mgmt"><i class="bi bi-people"></i>Gestisci gli utenti</a></li>
                  <li><a class="text-white text-decoration-none mb-3 fw-bold dropdown-item" routerLink="/customer_rentals"><i class="bi bi-bag"></i>Gestisci i noleggi</a></li>
                  <li><a class="text-white text-decoration-none mb-3 fw-bold dropdown-item" routerLink="/videogames_mgmt"> <i class="bi bi-controller"></i>Gestisci i videogiochi</a></li>
                </ul>
              </li>
              <!--SE LOGGATO, LOGOUT PER MODALITA MOBILE (XS) -->
              <button *ngIf="logged" class="text-secondary btn mt-5 text-decoration-underline d-block d-sm-none" (click)="logout()" >Logout</button>
              <!--SE LOGGATO, LINK A OFFCANVAS PER MODALITA NON MOBILE (>=sm) -->
              <li *ngIf="logged" class="nav-item d-none d-sm-block" data-bs-toggle="offcanvas" href="#offcanvasRight" role="button" aria-controls="offcanvasExample">
                <a class="nav-link"><i class="bi bi-person"></i></a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- SE LOGGATO, OFFSET -->
      <div class="offcanvas offcanvas-end px-5" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel" >
        <div class="offcanvas-header mt-3">
          <h5 class="offcanvas-title" id="offcanvasExampleLabel">Il tuo menu</h5>
          <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body d-flex flex-column justify-content-center align-items-start">
          <i class="bi bi-person-circle"></i>
          <!-- se cliente -->
          <ul *ngIf="user.ruolo !== 'admin'" class="nav flex-column mt-5 pt-5" >
            <li><i class="bi bi-gear"></i><a class="text-dark text-decoration-none mb-3 fw-bold" routerLink="./my_info">Il tuo profilo</a></li>
            <li><i class="bi bi-bell"></i><a class="text-dark text-decoration-none mb-3 fw-bold" routerLink="./my_notifications">Le tue notifiche</a></li>
            <li><i class="bi bi-cart"></i><a  class="text-dark text-decoration-none mb-3 fw-bold" routerLink="./my_rentals">I tuoi noleggi</a></li>
            <li><i class="bi bi-heart"></i><a class="text-dark text-decoration-none mb-3 fw-bold" routerLink="./my_wishlist">La tua wishlist</a></li>
          </ul>
          <!-- se amministratore -->
          <ul *ngIf="user.ruolo === 'admin'" class="nav flex-column mt-5 pt-5">
            <li><a class="text-dark text-decoration-none mb-3 fw-bold dropdown-item" routerLink="/customer_mgmt"><i class="bi bi-people"></i>Gestisci gli utenti</a></li>
            <li><a class="text-dark text-decoration-none mb-3 fw-bold dropdown-item" routerLink="/customer_rentals"><i class="bi bi-bag"></i>Gestisci i noleggi</a></li>
            <li><a class="text-dark text-decoration-none mb-3 fw-bold dropdown-item" routerLink="/videogames_mgmt"><i class="bi bi-controller"></i>Gestisci i videogiochi</a></li>
          </ul>
          <button class="text-secondary btn mt-5 text-decoration-underline" (click)="logout()">Effettua il logout</button>
        </div>
      </div>
    </header>
  `,
  styles: [`
    nav{
      height: 15vh;
      background:#1c1e20;
    }
    nav .nav-link{
      color: white;
    }
    nav .first_wrap .nav-link{
      margin-right: 30px;
      border-bottom: 2px solid rgb(70, 70, 70);
    }
    nav .nav-link:hover{
      color: white;
    }
    nav .navbar-toggler-icon{
      background-image: url("data:image/svg+xml;charset=utf8,%3Csvg viewBox='0 0 32 32' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath stroke='white' stroke-width='2' stroke-linecap='round' stroke-miterlimit='10' d='M4 8h24M4 16h24M4 24h24'/%3E%3C/svg%3E");
    }
    nav .navbar-toggler{
      border-color: transparent;
    }
    nav .closebtn{
      right: 25px;
      font-size: 45px;
    }
    nav .navbar-collapse{
      position: fixed;
      left: 0;
      background: #1c1e20;
      z-index: 200;
    }
    nav .navbar-collapse.collapsing{
      left: -75%;
      transition: height 0s ease;
    }
    nav .navbar-collapse.show{
      left: 0;
      transition: left 300ms ease-in-out;
    }
    nav .navbar-toggler.collapsed ~ .navbar-collapse {
      transition: left 500ms ease-in-out;
    }
    nav .dropdown-menu{
      background: #1c1e20;
      color:white;
      border: none;
    }
    .offcanvas a:focus{
      background:white;
    }
    

    @media(min-width: 540px){
      nav .navbar-collapse {
        position: relative;
      }
      nav .first_wrap .nav-link{
        border-bottom: none;
      }
      .offcanvas{
        background: white;
        width:35%;
      }
      .offcanvas .nav{
        border-top: 1px solid #9C9C9D;
      }
      .offcanvas i{
        font-size: 20px;
        color: #9C9C9D;
        margin-right: 20px;
      }
      .offcanvas i.bi-person-circle{
        font-size: 70px;
        color:black;
      }
      .offcanvas .offcanvas-header.button{
        top:10%;
        color:white;
      }
      .offcanvas a{
        font-size: 30px;
      }
    }
  `]
})
export class HeaderComponent implements OnInit {

  user : User = {}
  token : any
  logged: boolean = false;

  constructor(private _loginService : LoginService, private router : Router, private _userInfoService: UserInfoService) {
    this.token = _loginService.getToken()
  }

  ngOnInit(): void {
    //console.log(this.token)
    if(this._loginService.isLoggedIn()){
     this.logged = true;
     this._userInfoService.getUserProfile().subscribe(
       res=>{
        this.user = res['user']
       },
       err =>{
        alert(err)
       }
     );
    }else{
      this.logged = false;
    }

  }

  logout(){
    this._loginService.deleteToken();
    alert("Logout effettuato")
    this.router.navigateByUrl('/');
    window.location.reload();
  }
}
