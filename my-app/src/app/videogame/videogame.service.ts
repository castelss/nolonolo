import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class VideogameService {

  private baseUrl: string = "http://localhost:4000/videogames/";
  

  constructor(private _httpClient: HttpClient) { };

  getProduct(id) {
    return this._httpClient.get(this.baseUrl+id)
  }

  addVideo(videogioco){
    return this._httpClient.post(this.baseUrl+"wishlist",videogioco)
  }

  removeVideo(videogioco){
    return this._httpClient.post(this.baseUrl+"deleteWishlist",videogioco)
  }

  isAvaiable(rent){
    return this._httpClient.post(this.baseUrl+"disponibilita/",rent)
  }

  

  newReview(form){
    return this._httpClient.post(this.baseUrl+"review",form)
  }

  getUserProfile(){
    return this._httpClient.get("http://localhost:4000/users/")
  }
}