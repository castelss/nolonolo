import { Component, OnInit } from '@angular/core';
import { VideogameService } from './videogame.service';
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { LoginService } from '../login/login.service';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { Rent } from '../../../../backend/models/rent'
import { User } from '../../../../backend/models/rent'
import { Videogame } from '../../../../backend/models/videogames'
import Swal from 'sweetalert2';


@Component({
  selector: 'app-videogame',
  template: `
  <main class="container" *ngIf="videogame != undefined">
    <div class="row">
      <div class="col-md-8 p-4 order-2 order-sm-2 order-md-1 order-lg-1" id="details">
        <div class="mb-2">
          <h5 class="text-white">Descrizione</h5><hr>
          <p class="text-secondary">{{videogame.descrizione}}</p>
        </div>
        <div class="mb-2">
          <h5 class="text-white">Informazioni aggiuntive</h5><hr>
          <div class="table-responsive">
            <table class="table text-secondary table-borderless">
              <tbody>
              <tr>
                <th scope="row">Conservazione</th>
                <td>
                  <button type="button" class="text-secondary modal_button" data-bs-toggle="modal" data-bs-target="#modal_4conservation">
                    <i class="bi bi-info-circle"></i>
                  </button>
                </td>
                <td>{{videogame.conservazione}}</td>
              </tr>
              <tr>
                <th scope="row">Disponibilità</th>
                <td>
                  <button type="button" class="text-secondary modal_button" data-bs-toggle="modal" data-bs-target="#modal_4availability">
                    <i class="bi bi-info-circle"></i>
                  </button>
                </td>
                <td *ngIf="videogame.disponibile == true">si</td>
                <td *ngIf="videogame.disponibile != true">no</td>
              </tr>
              <tr>
                <th scope="row">Categoria</th>
                <td>
                  <button type="button" class="text-secondary modal_button" data-bs-toggle="modal" data-bs-target="#modal_4category">
                    <i class="bi bi-info-circle"></i>
                  </button>
                </td>
                <td>{{videogame.categoria}}</td>
              </tr>
              </tbody>
            </table>
          </div>

          <!--MODALE PER INFO SU CONSERVAZIONE -->

          <div class="modal fade" id="modal_4conservation" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Conservazione</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                  <p>Gentile cliente, con conservazione intendiamo lo stato di usura del videogioco a cui l'attributo si riferisce. I possibili indicatori di conservazione, in ordine decrescente, sono:</p>
                  <ol>
                    <li>Ottimo</li>
                    <li>Buono</li> 
                    <li>Scarso</li> 
                  </ol>
                  <p>Nel caso numero 3, il videogioco è automaticamente non disponibile al noleggio per motivi di restaurazione.
                  </p>
                </div>
              </div>
            </div>
          </div>

          <!--MODALE PER INFO SU DISPONIBILITA-->

          <div class="modal fade" id="modal_4availability" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Disponibilità</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                  <p>Gentile cliente, con disponibilità indichiamo la possibilità di poter noleggiare il videogioco cui l'attributo si riferisce. I valori associati a disponibilità sono:</p>
                  <ul>
                    <li>Sì : il videogioco è disponibile al noleggio.</li>
                    <li>No : il videogioco non è nelle condizioni per essere noleggiato.</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>

          <!--MODALE PER INFO SU CATEGORIA-->

          <div class="modal fade" id="modal_4category" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Categoria</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                  <p>Gentile cliente, con categoria intendiamo la macro-area tematica cui appartiene il videogioco.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="mb-2">
          <div class="d-flex justify-content-between align-items-center">
            <h5 class="text-white">Valutazioni e recensioni</h5><hr>
            <button class="btn text-white rounded-pill gradient_background" (click)="getReviewForm()"><i class="bi bi-plus"></i><i class="bi bi-pen"></i></button>
          </div>

          <!--DIV CON FORM PER RECENSIONE NASCOSTO FINCHE NON CLICCA SUL BOTTONE-->

          <div *ngIf="isShown_2">
            <h6 class="text-white">Lascia anche tu una recensione!</h6>
            <form method="post" (ngSubmit)="newReview()" [formGroup]="newReviewForm">
              <input type="hidden" value="ID VIDEOGIOCO"> <!--non si vede, è nascosto ma serve per il form-->
              <div class="form-floating mb-1">
                <select class="form-select" formControlName="stars">
                  <option value="1" selected>1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                </select>
                <label for="floatingSelect">Seleziona il numero di stelle</label>
              </div>
              <div class="form-floating mb-1">
                <textarea class="form-control" name="text" id="text" style="height: 60px" formControlName="testo"></textarea>
                <label for="floatingTextarea2">Scrivi il testo della recensione</label>
              </div>
              <button type="submit" class="btn gradient_background text-white" name='review' id='review'>Invia</button>
            </form>
          </div>

          <hr>
          <div class="mb-1" *ngFor="let recensione of recensioni">
            <div class="d-flex justify-content-start align-items-center">
              <span class="text-white">{{recensione.user.nome}}</span>
              <div class="stars ms-3" *ngIf="recensione.stelle ==1">
                <i class="bi bi-star-fill" style="color:yellow"></i> 
                <i class="bi bi-star-fill"></i>
                <i class="bi bi-star-fill"></i>
                <i class="bi bi-star-fill"></i>
                <i class="bi bi-star-fill"></i>
              </div>
              <div class="stars ms-3" *ngIf="recensione.stelle ==2">
                <i class="bi bi-star-fill" style="color:yellow"></i> 
                <i class="bi bi-star-fill" style="color:yellow"></i>
                <i class="bi bi-star-fill"></i>
                <i class="bi bi-star-fill"></i>
                <i class="bi bi-star-fill"></i>
              </div>
              <div class="stars ms-3" *ngIf="recensione.stelle ==3">
                <i class="bi bi-star-fill" style="color:yellow"></i> 
                <i class="bi bi-star-fill" style="color:yellow"></i>
                <i class="bi bi-star-fill" style="color:yellow"></i>
                <i class="bi bi-star-fill"></i>
                <i class="bi bi-star-fill"></i>
              </div>
              <div class="stars ms-3" *ngIf="recensione.stelle ==4">
                <i class="bi bi-star-fill" style="color:yellow"></i> 
                <i class="bi bi-star-fill" style="color:yellow"></i>
                <i class="bi bi-star-fill" style="color:yellow"></i>
                <i class="bi bi-star-fill" style="color:yellow"></i>
                <i class="bi bi-star-fill"></i>
              </div>
              <div class="stars ms-3" *ngIf="recensione.stelle ==5">
                <i class="bi bi-star-fill" style="color:yellow"></i> 
                <i class="bi bi-star-fill" style="color:yellow"></i>
                <i class="bi bi-star-fill" style="color:yellow"></i>
                <i class="bi bi-star-fill" style="color:yellow"></i>
                <i class="bi bi-star-fill" style="color:yellow"></i>
              </div>
            </div>
            <p class="text-secondary text">{{recensione.testo}}</p>
          </div>
        </div>
      </div>

      <div class="col-md-4 p-4 order-1 order-sm-1 order-md-2 order-lg-2 p-4">
        <div class="pb-1">
          <h2 class="text-white py-3 py-lg-1 name">{{videogame.nome}}</h2>
          
          <img [src]="videogame.image" [alt]="videogame.nome" class="img-fluid" width="100%">
        </div>
        <div class="d-flex justify-content-between align-items-center mt-2">
          <span class="text-white text-center rounded text-dark">{{videogame.pzFissoBaby | number : '1.2-2'}} € 
            <button type="button" class="modal_button text-white" data-bs-toggle="modal" data-bs-target="#modal_4prize">
              <i class="bi bi-info-circle"></i>
            </button>
          </span>
          <div class="link-group" role="group">
            <button *ngIf="videogame.disponibile === true" class="btn text-white me-3 border" (click)="getRentalForm()"><i class="bi bi-cart"></i></button>
            <button *ngIf="isPref" class="btn text-white border" (click)="delWishlist(videogame._id)"><i class="bi bi-heart-fill"></i></button>
            <button *ngIf="!isPref" class="btn text-white border" (click)="wishlist(videogame._id)"><i class="bi bi-heart"></i></button>
          </div>
        </div>

        <!--DIV CON FORM PER NOLEGGIO NASCOSTO FINCHE NON CLICCA SUL BOTTONE-->

        <div *ngIf="isShown_1" class="mt-3 p-1">
          <h6 class="text-white">Richiedi il noleggio di questo videogioco!</h6>
          <form #availableForm="ngForm" (ngSubmit)="isAvailable(availableForm)"class="rentalForm p-3 mt-3 rounded">
            <label class="form-label text-start">Seleziona una data di inizio noleggio</label>
            <input type="date" class="w-100  mb-3 rentalForm_input" name="data" #dataInizio="ngModel"  [(ngModel)]="rent.dataInizio">
            <label class="form-label text-start" >Seleziona una data di fine noleggio</label>
            <input type="date" class="w-100  mb-3 rentalForm_input" name="data" #dataFine="ngModel"  [(ngModel)]="rent.dataFine">
            <button  type="submit" class="btn text-dark border border-dark rounded mt-3 text-center w-100 ">Verifica disponibilità</button>
          </form>
        </div>

        <!--MODALE PER INFO SU PREZZO FISSO-->

        <div class="modal fade" id="modal_4prize" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                <p>Gentile cliente, le ricordiamo che il prezzo segnalato è riferito <b>al solo prezzo fisso del videogioco</b>. Al momento del pagamento di fine noleggio verrà calcolato (e aggiunto al prezzo fisso per calcolare il prezzo totale) un prezzo variabile basato sulla durata del noleggio stesso.</p>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </main>
  `,
  styles: [
    `
    
    main{
      padding-top:60px;
    }
    hr{
      color:white;
    }
    button{
      border:none;
    }
    button:focus{
      outline: none !important;
      box-shadow: 0 0 0 transparent;
    }
    .rentalForm{
      background:white;
    }
    .rentalForm .rentalForm_input{
      border: none;
      border-bottom: 1px solid black;
    }
    .rentalForm .rentalForm_input:focus { 
      outline: none !important;
    }
    .stars i{
      font-size: 10px;
    }
    .prize{
      font-size: 25px;
    }
    .review .text{
      font-size: 12px;
    }
    .modal-body{
      font-size:15px;
    }
    .modal_button{
      background:#111111;
      border:none;
      font-size: 10px;
    }
    .modal_button:focus { 
      outline: none !important;
      box-shadow: 0 0 10px black;
    }
    @media(min-width: 576px){
      main{
        padding-top: 100px;
      }
      #details{ 
        margin-top:100px;
      }
      .review_button{
        width:80%;
      }
    }
    `
  ]
})
export class VideogameComponent implements OnInit {
  
  isShown_1: boolean = false ;
  isShown_2: boolean = false ;

  getRentalForm(){
    this.isShown_1 = ! this.isShown_1;
  }
  getReviewForm(){
    this.isShown_2 = ! this.isShown_2;
  }
  
  newReviewForm : FormGroup
  isPref : boolean = false
  preferiti : any = []
  user : User
  videogame : Videogame = {}
  recensioni : any = []
    id : any = ''
    rent : Rent = { dataInizio: Date.now(),
                    dataFine: Date.now(),
                    videogioco: ''
                }
  


  constructor(private _videogameService: VideogameService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private _loginService: LoginService, private formBuilder: FormBuilder) { 
      this.newReviewForm = this.formBuilder.group({id: "",stars: "", testo:""})
      //this.videogame.recensione.user ={};
    }
  
    


    isAvailable(form : NgForm){
      this.rent.videogioco = this.id
      //console.log(this.rent)
      if(!this._loginService.isLoggedIn()){
        alert("Effettuare prima l'accesso");
        this.router.navigateByUrl('/login');
      }else{
        this._videogameService.isAvaiable(this.rent).subscribe(
          res => {
            if(res['code']==1){
              Swal.fire('OTTIMO :)', "Noleggio avvenuto con successo", 'success')
              this.router.navigateByUrl('/my_rentals');
              
            }else{
              Swal.fire('OOPS :(', "Date non disponibili, prova a cambiarle", 'error')
              form.reset();            
            }
            
            //chiamata per creazione rent
            //dopo mi sposto alla pagina degli ordini
            
          },
          err => {
            form.reset()
            alert(err.message);
          }
        )
      } 
    }

    wishlist(id){
      if(!this._loginService.isLoggedIn()){
        alert("Effettuare prima l'accesso");
        this.router.navigateByUrl('/login');
      }else{
        this._videogameService.addVideo(this.videogame).subscribe(
          res=>{
            //alert(res['message'])
            window.location.reload()
          },
          err =>{
            alert(err)
          }
        )
      }
    }

    delWishlist(id){
      if(!this._loginService.isLoggedIn()){
        alert("Effettuare prima l'accesso");
        this.router.navigateByUrl('/login');
      }else{
        this._videogameService.removeVideo(this.videogame).subscribe(
          res=>{
            //alert(res['message'])
            window.location.reload()
          },
          err =>{
            alert(err)
          }
        )
      }
    }


  ngOnInit(): void {
   
    this.id = this.activatedRoute.snapshot.params['id']

    if(this.id){
      this._videogameService.getProduct(this.id).subscribe(res =>{
        console.log(res)
        if(res['success']=true){ 
          this.videogame=res['videogame']
          this.recensioni = this.videogame.recensione
          console.log(this.recensioni)
        }
        else{
          console.log(res['error'])
        }
      })
    }
    if(this._loginService.isLoggedIn()){
      this._videogameService.getUserProfile().subscribe(
        res=>{
         this.user = res['user']
         console.log(this.user)
         this.preferiti = this.user.preferiti
         this.preferiti.forEach((pref) => {
           if (pref==this.videogame._id){
             this.isPref =true 
           }
         });
         console.log(this.isPref)
        },
        err =>{
         alert(err)
        }
      );
     }
  }
   newReview(){
    
    if(!this._loginService.isLoggedIn()){
      alert("Effettuare prima l'accesso");
      this.router.navigateByUrl('/login');
    }else{
      this.newReviewForm.value['id'] = this.id;
      this._videogameService.newReview(this.newReviewForm.value).subscribe(
        res =>{
          this.newReviewForm.reset()
          window.location.reload()
        },
        err =>{
          this.newReviewForm.reset()
          alert(err)
        }
      )
    }
  }

}
