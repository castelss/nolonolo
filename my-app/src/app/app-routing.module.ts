import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdmAllOrdersComponent } from './adm-all-orders/adm-all-orders.component';
import { AdmManageOrdersComponent } from './adm-manage-orders/adm-manage-orders.component';
import { AdmManageUsersComponent } from './adm-manage-users/adm-manage-users.component';
import { AdmManageVideogamesComponent } from './adm-manage-videogames/adm-manage-videogames.component';
import { AuthGuard } from './auth/auth.guard';
import { CatalogComponent } from './catalog/catalog.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { RegistrationComponent } from './registration/registration.component';
import { UserOrdersComponent } from './user-orders/user-orders.component';
import { UserWishlistComponent } from './user-wishlist/user-wishlist.component';
import { VideogameComponent } from './videogame/videogame.component';
import { UserInfoComponent } from './user-info/user-info.component';
import { UserNotificationsComponent } from './user-notifications/user-notifications.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'catalog',
    component: CatalogComponent
  },
  {
    path: 'catalog/videogame/:id',
    component: VideogameComponent
  },
  {
    path: 'my_wishlist',
    component: UserWishlistComponent,
    canActivate: [AuthGuard] 
  },
  {
    path: 'my_info',
    component: UserInfoComponent,
    canActivate: [AuthGuard] 
  },
  {
    path: 'my_notifications',
    component: UserNotificationsComponent,
    canActivate: [AuthGuard] 
  },
  {
    path: 'my_rentals',
    component: UserOrdersComponent,
    canActivate: [AuthGuard] 
  },
  {
    path:'my_rentals/order_details/:id',
    component: OrderDetailsComponent,
    canActivate: [AuthGuard] 
  },
  {
    path: 'customer_rentals',
    component:AdmAllOrdersComponent
  },
  {
    path: 'customer_rentals/order_mgmt/:id',
    component: AdmManageOrdersComponent
  },
  {
    path: 'customer_mgmt',
    component: AdmManageUsersComponent
  },
  {
    path: 'videogames_mgmt',
    component: AdmManageVideogamesComponent
  },
  {
    path:'login',
    component: LoginComponent
  },
  {
    path: 'registration',
    component: RegistrationComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
