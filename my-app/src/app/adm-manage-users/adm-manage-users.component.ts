import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { RegistartionService } from '../registration/registartion.service';
import { AdmManageUsersService } from './adm-manage-users.service';
import { User } from '../../../../backend/models/user'

@Component({
  selector: 'app-adm-manage-users',
  template: `
  <main>
    <div class="container d-flex flex-column justify-content-center pt-5 mb-4">
    <h3 class="text-white mb-1 px-3">Gestisci gli utenti della piattaforma</h3> 
      <div class="row row-cols-1 row-cols-lg-2 row-cols-md-1 mt-3 first_row d-flex justify-content-center">
        <div class="col d-flex justify-content-center align-items-center p-4">
          <div class="form_wrapper rounded px-1 py-3 p-lg-3 ">
            <h3 class="px-3 text-dark">Iscrivi un utente</h3>
            <form method="post" class="mt-2 w-100 p-3" (ngSubmit)="newUser()" [formGroup]="newUserForm">
              <div class="form-floating mb-1">
                  <input type="text" formControlName="nome" placeholder="Nome" class="form-control" name=nome>
                  <label>Nome dell'utente</label>
              </div>
              <div class="form-floating mb-1">
                  <input type="text" formControlName="cognome" placeholder="Cognome" class="form-control" name=cognome>
                  <label>Cognome dell'utente</label>
              </div>
              <div class="form-floating mb-1">
                  <input type="text" placeholder="Username" formControlName="username" class="form-control" name=username>
                  <label>Username dell'utente</label>
              </div>
              <div class="form-floating mb-1">
                  <input type="email" placeholder="nome.cognome@libero.it" formControlName="email" class="form-control"name=email>
                  <label>Email dell'utente</label>
              </div>
              <div class="form-floating mb-1">
                  <input type="password" placeholder="123456" class="form-control" formControlName="password" name=password>
                  <label>Password dell'utente</label>
              </div>
              <div class="form-floating mb-1">
                  <input type="date" placeholder="19/11/1998" class="form-control" formControlName="dataNascita" name=dataNascita>
                  <label>Data di nascita dell'utente</label>
              </div>
              <button type="submit" name='submit' class="btn text-white w-100"> Crea  </button>
            </form>
          </div>
        </div>
        <div class="col d-flex justify-content-center align-items-center">
          <div class="row row-cols-1 h-100 w-100 d-flex justify-content-center align-items-center">
            <div class="col">
              <div class="form_wrapper rounded px-1 py-3 p-lg-3 ">
                <h3 class="px-3 text-dark">Blocca o sblocca un utente</h3>
                <form method="post" class="mt-1 w-100 px-3" [formGroup]="lockUserForm" >
                <label>Seleziona il nome dell'utente</label>
                  <select class="form-select form-select-sm mb-3" aria-label=".form-select-lg" formControlName="_id" >
                  <option  *ngFor="let user of users" [value]="user._id">{{user.nome}} {{user.cognome}}</option>
                  </select>
                  <div class="d-flex justify-content-between">
                  <button type="submit" name='submit' class="btn text-white w-100  me-3" (click)="lockUser()"> Bloccalo  </button> 
                  <button type="submit" name='submit' class="btn text-white w-100 " (click)="unlockUser()"> Sbloccalo  </button>
                  </div>
                </form>
              </div>
            </div>
            <div class="col mt-4 mt-sm-4 mt-md-4 mt-lg-0">
              <div class="form_wrapper rounded px-1 py-3 p-lg-3 ">
                <h3 class="px-3 text-dark">Scrivi ad un utente</h3>
                <form method="post" class="mt-1 w-100 px-3" (ngSubmit)="sendMsg()" [formGroup]="msgForm">
                  <label>Seleziona il nome dell'utente destinatario</label>
                  <select class="form-select form-select-sm mb-3" aria-label=".form-select-lg" formControlName="id">
                    <option   *ngFor="let user of users" [value]="user._id">{{user.nome}} {{user.cognome}}</option>
                  </select>
                  <div class="mb-3">
                    <label for="exampleFormControlTextarea1" class="form-label">Digita il contenuto del messaggio</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="1" formControlName="testo"></textarea>
                  </div>
                  <button type="submit" name='submit' id='submit' class="btn text-white w-100"> Invia  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="col p-4">
            <div class="form_wrapper rounded px-1 py-3 p-lg-3 ">
            <h3 class="px-3 text-dark">Modifica le informazioni di un utente</h3>
            <form method="post" class="mt-2 w-100 p-3" (ngSubmit)="updateUser()" [formGroup]="updateUserForm">
              <label>Seleziona l'email</label>
              <select class="form-select form-select-sm mb-3" aria-label=".form-select-lg" formControlName="id" >
              
              <option  *ngFor="let user of users" [value]="user._id">{{user.email}}</option>
              </select>
              <div class="form-floating mb-1">
                  <input type="text" formControlName="nome" placeholder="nome" class="form-control" name=nome>
                  <label>Modifica il nome</label>
              </div>
              <div class="form-floating mb-1">
                  <input type="text" formControlName="cognome" placeholder="Cognome" class="form-control" name=cognome>
                  <label>Modifica il cognome</label>
              </div>
              <div class="form-floating mb-1">
                  <input type="text" placeholder="Username" formControlName="username" class="form-control" name=username>
                  <label>Cambia username</label>
              </div>
              <div class="form-floating mb-3">
                  <input type="date" placeholder="19/11/1998" class="form-control" formControlName="dataNascita" name=dataNascita>
                  <label>Cambia data di nascita</label>
              </div>
              <div class="form-check mb-3">
                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" formControlName="azzera">
                <label class="form-check-label" for="flexCheckDefault">
                  Spunta per azzerare i punti all'utente selezionato
                </label>
              </div>
              <label>Cambia ruolo</label>
              <select class="form-select form-select-sm mb-3" aria-label=".form-select-lg" formControlName="ruolo" >
              <option value="admin">Amministratore</option>
              <option value="user">Cliente</option>
              </select>
              <button type="submit" name='submit' class="btn text-white w-100"> Modifica  </button>
            </form>
          </div>
        </div>
        <div class="col p-4 d-flex flex-column align-items-center justify-content-center">
            <div class="form_wrapper h-100 rounded px-1 py-3 p-lg-3 position-relative">
            
            <div class="vertical-scrollable position-absolute top-0 start-0 end-0 bottom-0 d-flex flex-column justify-content-center align-items-start px-4 py-2">
            <h3 class="mt-2 mb-5">Visualizza la lista degli utenti </h3>
            <div class="wrapper mt-2">
            
            <ul class="list-group">
              <li class="list-group-item" *ngFor="let user of users">{{user.username}} : {{user.ruolo}}</li>
            </ul>
            </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </main>
  `,
  styles: [
    `
    input:-webkit-autofill {
      box-shadow: 0 0 0 1000px white inset !important;
      -webkit-text-fill-color: black !important;
  }
  input:focus{
    outline: none !important;
    box-shadow: 0 0 0 transparent;
    background:white;
  }

    main{
      padding-top: 5vh;
    }
    .form_wrapper{
      background:white;
      width:99%;
    }
    button,a{
      background:#64737c;
    }
    .vertical-scrollable> .wrapper {
      width: 100%;
      height: 90%;
      overflow-y: scroll; 
    }
    `
  ]
})
export class AdmManageUsersComponent implements OnInit {
  newUserForm: FormGroup
  updateUserForm: FormGroup 
  lockUserForm: FormGroup 
  msgForm: FormGroup 
  users : User

  constructor(private router : Router, 
    private regService : RegistartionService,
    private admUsersService : AdmManageUsersService,
    private formBuilder: FormBuilder) {
      this.newUserForm = this.formBuilder.group({nome: "", cognome:"", username:"", email:"", password:"", dataNascita: "" })
      this.updateUserForm = this.formBuilder.group({id:"",nome: "", cognome:"", username:"", email:"", password:"", dataNascita: "",azzera:"",ruolo:"" })
      this.lockUserForm = this.formBuilder.group({_id: ""})
      this.msgForm =this.formBuilder.group({id:"", testo:""})
     }

  ngOnInit(): void {
    this.admUsersService.getUsers().subscribe(users =>{
      this.users = users;
      console.log(users)
    })
  }

  newUser(){
    this.regService.register(this.newUserForm.value).subscribe(
      res => {
        this.newUserForm.reset();
        alert("Utente inserito correttamente")
      },
      err =>{
        this.newUserForm.reset();
        alert(err)
      }
    )
  };

  lockUser(){
    this.admUsersService.lockUser(this.lockUserForm.value).subscribe(
      res => {
        this.lockUserForm.reset();
        alert("Utente Bloccato");
      }
    )
  }
  unlockUser(){
    this.admUsersService.unlockUser(this.lockUserForm.value).subscribe(
      res => {
        this.lockUserForm.reset();
        alert("Utente Sbloccato");
      }
    )
  }
  updateUser(){
    this.admUsersService.updateUser(this.updateUserForm.value).subscribe(
      res => {
        this.updateUserForm.reset();
        alert("Utente Modificato");
      },
      err =>{
        alert(err);
      }
    )
  }
  sendMsg(){
    this.admUsersService.sendMsg(this.msgForm.value).subscribe(
      res => {
        this.msgForm.reset();
        alert("Messaggio inviato");
      },
      err =>{
        alert(err);
      }
    )
  }

}
