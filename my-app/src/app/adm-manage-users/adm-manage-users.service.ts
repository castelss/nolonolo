import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AdmManageUsersService {
  private baseUrl: string = "http://localhost:4000/users/";

  constructor(private _httpClient: HttpClient) { };

  getUsers(){
    return this._httpClient.get(this.baseUrl+"allUsers");
  }

  lockUser(_id : any){
    return this._httpClient.post(this.baseUrl+"lockUser", _id);
  }
  unlockUser(_id : any){
    return this._httpClient.post(this.baseUrl+"unlockUser", _id);
  }

  updateUser(form){
    return this._httpClient.post(this.baseUrl+"admin/update", form);
  }

  sendMsg(form){
    return this._httpClient.post(this.baseUrl+"admin/msg", form);
  }
}
