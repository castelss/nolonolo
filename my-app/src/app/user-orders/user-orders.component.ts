import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from '../login/login.service';
import { User } from '../../../../backend/models/user.js';
import { Rent } from '../../../../backend/models/rent';
import { UserInfoService } from '../user-info/user-info.service';
import { UserOrdersService } from './user-orders.service';

@Component({
  selector: 'app-user-orders',
  template: `
  <main>
    <div class="container-fluid container-lg">
      <h3 class="text-white">I tuoi ordini</h3>
      <div class="form-group w-100 mt-4">
        <input type="text" class="form-control border-bottom border-bottom" placeholder="Filtra gli ordini.." [(ngModel)]="filterTerm">
      </div>
      <div class="row row-cols-1 row-cols-lg-3 mt-5"> <!--*ngIf="rents != undefined"-->
        <div *ngFor="let rent of rents" class="col-sm-4 d-flex justify-content-start align-items-center"> 
          <a routerLink="./order_details/{{rent._id}}" class="text-decoration-none"> 
            <div class="card border-0 w-100">
              <img [src]="rent.videogioco.image" [alt]="rent.videogioco.nome" class="card-img-top">
              <p class="text-white px-3"></p>
              <table class="table">
                <tbody>
                  <tr class="text-white">
                    <th scope="row">ORDINE</th>
                    <td>{{rent._id}}</td>
                  </tr>
                  <tr class="text-white">
                    <th scope="row">STATO</th>
                    <td>{{rent.stato}}</td>
                  </tr>
                  <tr class="text-white">
                    <th scope="row">OGGETTO</th>
                    <td>{{rent.videogioco.nome}}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </a>
        </div>
      </div>
    </div>
  </main>
  `,
  styles: [
    `
    .card{
      background:#111111;
    }
    main{
      padding-top: 15vh;
    }
    input{
      outline:none !important;
      border:none;
      background:#111111;
      border-radius:0px;
    }
    ::placeholder {
      color: white;
    }
    input.form-control{
      color:white;
      font-size:12px;
    }
    .form-group{
      width:80%;
    }
    input:focus{
      outline:none !important;
      box-shadow: 0 0 0 transparent;
      background:#111111;
    }

    `
  ]
})
export class UserOrdersComponent implements OnInit {
  filterTerm;

  user: User 
  rents : Rent
  constructor(private _loginService: LoginService,private router : Router, private _userInfoService: UserInfoService,
    private activatedRoute : ActivatedRoute, private _userOrdesService : UserOrdersService) { }

  ngOnInit(): void {
    this._userInfoService.getUserProfile().subscribe(
      res => {
        //console.log(res['user'])
        this.user = res['user'];
      },
      err => { 
        alert("utente non trovato");
        this.router.navigateByUrl('/login')
      }
    );

      this._userOrdesService.getRents().subscribe(
        res =>{
          //console.log(res['rents'])
          this.rents = res['rents'];
        },
        err => { 
          console.log(err);
        }
      );
  }

}

