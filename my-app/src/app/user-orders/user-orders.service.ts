import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserOrdersService {

  private baseUrl: string = "http://localhost:4000/noleggi/";

  constructor(private _httpClient: HttpClient) { }

  getRents() {
    return this._httpClient.get(this.baseUrl);
  }

  getRent(id){
    return this._httpClient.get(this.baseUrl+id);
  }

}
