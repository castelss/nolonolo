import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Videogame } from '../../../../backend/models/videogames';
import { CatalogService } from '../catalog/catalog.service';
import { VideogameService } from '../videogame/videogame.service';
import { AdmManageVideogamesService } from './adm-manage-videogames.service';

@Component({
  selector: 'app-adm-manage-videogames',
  template: `
  <main>
    <div class="container-fluid container-lg d-flex flex-column justify-content-center pt-5 mb-4">
    <h3 class="text-white mb-1 px-3">Gestisci i videogiochi</h3>
      <div class="row row-cols-1 row-cols-lg-2 row-cols-md-1 first_row d-flex justify-content-center">
        <div class="col d-flex justify-content-center align-items-center p-4">
          <div class="form_wrapper rounded p-3 h-100 d-flex flex-column justify-content-center">
            <h5 class="px-3 text-dark">Inserisci un nuovo videogioco</h5>
            <form method="post" class="w-100 p-3" (ngSubmit)="newVideogame()" [formGroup]="form">
              <label>Inserisci il nome del videogioco</label>
              <textarea class="form-control mb-2" rows="1" 
              name="nome" required formControlName="nome"  ></textarea>
              <label>Digita il prezzo fisso del videogioco</label>
              <textarea class="form-control mb-2" rows="1"
              name="pzFissoBaby" required formControlName="pzFissoBaby"></textarea>
              <label>Seleziona la categoria del videogioco</label>

              <select class="form-select form-select-sm mb-3" aria-label=".form-select-lg"
              name="categoria" formControlName="categoria">
                <option value="1">Avventura</option>
                <option value="2">Sport</option>
                <option value="3">Musica</option>
                <option value="4">Educazione</option>
              </select>
              <label>Seleziona lo stato del videogioco</label>
              <select name="conservazione" required formControlName="conservazione"
              class="form-select form-select-sm mb-3" aria-label=".form-select-lg">
                <option value="1">Buono</option>
                <option value="2">Scarso</option>
                <option value="2">Ottimo</option>
              </select>
              <label for="formFileSm" class="form-label"  >Inserisci un'immagine del videogioco</label>
              <input class="form-control form-control-sm mb-3" id="formFileSm" type="file" #filepicker (change) = "PickedImage($event)">
              <button type="submit" name='submit' class="btn text-white w-100"> Aggiungilo  </button>
            </form>
          </div>
        </div>
        <div class="col d-flex justify-content-center align-items-center p-4">
          <div class="form_wrapper rounded p-3 h-100 d-flex flex-column justify-content-center">
            <h5 class="px-3 text-dark">Modifica un videogioco del catalogo</h5>
            <p class="px-3 text-secondary">Compila SOLO i campi che intendi modificare</p>
            <form method="post" class="w-100 px-3" (ngSubmit)="updateVideogame()" [formGroup]="updateForm">
              <label>Seleziona l'identificativo del videogioco</label>
              <select class="form-select form-select-sm mb-3" aria-label=".form-select-lg" formControlName="id">
              <option value="0" selected></option>  
              <option *ngFor="let videogame of videogames" [value]="videogame._id">{{videogame.nome}}</option>
              </select>
              <label>Digita un nuovo nome</label>
              <textarea class="form-control mb-2" rows="1" formControlName="nome"> </textarea>
              <label>Digita un nuovo prezzo fisso</label>
              <textarea class="form-control mb-2" rows="1" formControlName="pzFissoBaby"> </textarea>
              <label>Cambia la categoria del videogioco</label>
              <select class="form-select form-select-sm mb-3" aria-label=".form-select-lg" formControlName="categoria">
              
                <option value="Avventura">Avventura</option>
                <option value="Sport">Sport</option>
                <option value="Musica">Musica</option>
                <option value="Educazione">Educazione</option>
              </select>
              <label>Cambia lo stato del videogioco</label>
              <select class="form-select form-select-sm mb-3" aria-label=".form-select-lg" formControlName="conservazione">
                
                <option value="Buono">Buono</option>
                <option value="Scarso">Scarso</option>
                <option value="Ottimo">Ottimo</option>
              </select>
              <button type="submit" name='submit' class="btn text-white w-100"> Modifica  </button>
            </form>
          </div>
        </div>
        <div class="col d-flex justify-content-center align-items-center p-4">
          <div class="form_wrapper rounded p-3 h-100 d-flex flex-column justify-content-center">
            <h5 class="px-3 text-dark">Cancella videogioco non noleggiato</h5>
            <form method="post" class="w-100 px-3"(ngSubmit)="deleteVg()" [formGroup]="deleteForm">
              <label>Seleziona l'identificativo del videogioco che intendi mettere in saldo</label>
              <select class="form-select form-select-sm mb-3" aria-label=".form-select-lg"formControlName="id">
                <option *ngFor="let videogame of videogames" [value]="videogame._id" (change)="getVG(videogame._id)" > 
                  <h6 *ngIf="videogame.noleggi ===0">{{videogame.nome}} </h6></option>
              </select>
              <button type="submit" name='submit' class="btn text-white w-100"> Cancella  </button>
            </form>
          </div>
        </div>
        <div class="col d-flex justify-content-center align-items-center p-4">
          <div class="form_wrapper rounded p-3 h-100 d-flex flex-column justify-content-center">
            <h5 class="px-3 text-dark">Rendi un videogioco non disponibile</h5>
            <form method="post" class="w-100 px-3" (ngSubmit)="cambiaStato()" [formGroup]="cambiaStatoForm">
              <label>Seleziona l'identificativo del videogioco</label>
              <select class="form-select form-select-sm mb-3" aria-label=".form-select-lg" formControlName="id">
                <option *ngFor="let videogame of videogames" [value]="videogame._id">{{videogame.nome}} : {{videogame.disponibile}}</option>
                <option value="null" selected></option>
              </select>
              <button type="submit" name='submit' class="btn text-white w-100"> Cambia Stato  </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </main>
  `,
  styles: [
    `
    main{
      padding-top: 5vh;
    }
    .form_wrapper{
      background:white;
      width:99%;
    }
    button{
      background:#64737c;
    }
    input:focus{
      outline: none !important;
      box-shadow: 0 0 0 transparent;
      background:white;
    }
    
    `
  ]
})
export class AdmManageVideogamesComponent implements OnInit {

  error : any = null
  form: FormGroup 
  video : Videogame 
  updateForm : FormGroup
  cambiaStatoForm : FormGroup
  deleteForm : FormGroup
  videogames : Videogame = [] 
  //videogame : Videogame = {nome: "", pzFissoBaby:"", categoria:"", conservazione: "", imagePath: ""}; 

  constructor(private router : Router,
    public _admManageVgService: AdmManageVideogamesService,
    private formBuilder: FormBuilder, private _catalogService : CatalogService, private _videogameService : VideogameService) {
      this.form = this.formBuilder.group({nome: "", pzFissoBaby:"", categoria:"", conservazione: "", imagePath: null})
      this.updateForm = this.formBuilder.group({id: "",nome: "", pzFissoBaby:"", categoria:"", conservazione: ""})
      this.cambiaStatoForm = this.formBuilder.group({id: ""})
      this.deleteForm = this.formBuilder.group({id: ""})
     }

  ngOnInit(): void {
    this._catalogService.getVideoGames().subscribe(videogames =>{
      console.log(videogames)
      this.videogames = videogames;
    })
  }

  PickedImage(event: Event){  
    const target =event.target as HTMLInputElement;
    const files = target.files;
    if (!files ||!files[0])
      return;
    const file = files[0];
    this.form.patchValue({
      imagePath: file
    });
    this.form.get('imagePath')?.updateValueAndValidity();
      
    
  }  

  newVideogame(){
    console.log(this.form.value);
    this._admManageVgService.insertVideogame(this.form.value.nome, this.form.value.pzFissoBaby, this.form.value.categoria,
      this.form.value.conservazione, this.form.value.imagePath).subscribe(
      res => {
        this.form.reset()
        Swal.fire('OK :)', 'Videogioco inserito con successo', 'success')
        this.router.navigate(['/videogames_mgmt']);
      },
      err => {
        this.form.reset()
        Swal.fire('OOPS :(', 'Qualcosa è andato storto', 'error')
        this.router.navigate(['/videogames_mgmt']);
      }
    );
  }

  updateVideogame(){
    this._admManageVgService.updateVideogame(this.updateForm.value).subscribe(
      res => {
        this.updateForm.reset()
        Swal.fire('OK :)', 'Videogioco aggiornato con successo', 'success')
        this.router.navigate(['/videogames_mgmt']);
      },
      err => {
        this.updateForm.reset()
        Swal.fire('OK :)', 'Qualcosa è andato storto', 'error')
        this.router.navigate(['/videogames_mgmt']);
      }
    );
  }
  cambiaStato(){
    this._admManageVgService.changeVg(this.cambiaStatoForm.value).subscribe(
      res => {
        this.updateForm.reset()
        Swal.fire('OK :)', 'Videogioco aggiornato con successo', 'success')
        this.router.navigate(['/videogames_mgmt']);
      },
      err => {
        this.updateForm.reset()
        Swal.fire('OK :)', 'Qualcosa è andato storto', 'error')
        this.router.navigate(['/videogames_mgmt']);
      }
    );
  }

  deleteVg(){
    this._admManageVgService.deleteVg(this.deleteForm.value).subscribe(
      res => {
        this.updateForm.reset()
        Swal.fire('OK :)', 'Videogioco eliminato con successo', 'success')
        this.router.navigate(['/videogames_mgmt']);
      },
      err => {
        this.updateForm.reset()
        Swal.fire('OK :)', 'Qualcosa è andato storto', 'error')
        this.router.navigate(['/videogames_mgmt']);
      }
    );
  }

  getVG(id){
    this._videogameService.getProduct(id).subscribe(videogame =>{
      this.video = videogame;
    })
  }
}
