import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { title } from 'process';
import { Videogame } from '../../../../backend/models/videogames';

@Injectable({
  providedIn: 'root'
})
export class AdmManageVideogamesService {

  
  private baseUrl: string = "http://localhost:4000/videogames/";

  constructor(private _httpClient: HttpClient) { };

  
  insertVideogame(nome: string, pzFissoBaby: string, categoria: string, conservazione: string, image: File) {
    //const postData = new FormData();
    //console.log(image)
    var formData: any = new FormData();
    formData.append("nome", nome);
    formData.append("categoria", categoria);
    formData.append("conservazione", conservazione);
    formData.append("image", image);
    formData.append("pzFissoBaby", pzFissoBaby);
    console.log(formData);
    return this._httpClient.post(this.baseUrl+"upload", formData)
  }

  updateVideogame(form){
    return this._httpClient.post(this.baseUrl+"update", form)
  }

  changeVg(form){
    return this._httpClient.post(this.baseUrl+"cambiaStato", form)
  }

  deleteVg(form){
    return this._httpClient.post(this.baseUrl+"delete", form)
  }

}
