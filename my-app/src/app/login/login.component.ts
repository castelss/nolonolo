import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-login',
  template: `
  <main>
    <div class="container-fluid container-lg">
      <div class="row d-flex justify-content-start align-items-center">
        <div class="col-sm-6 d-flex align-items-center justify-content-start rounded p-sm-5 ">
          <form #loginForm="ngForm" (ngSubmit)="onSubmit(loginForm)" method="post" class="px-3">
            <h2 class="text-white mb-1">Pronto per la prossima avventura?</h2>
            <div class="form-floating mb-md-2">
              <input type="email" class="form-control border-bottom border-white" name="email" #email="ngModel"  [(ngModel)]="model.email" required>
              <label class="text-white">Inserisci la tua e-mail</label>
            </div>
            <div class="form-floating mb-md-2">
              <input type="password" class="form-control border-bottom border-white" name="password" #password="ngModel"  [(ngModel)]="model.password">
              <label class="text-white">Inserisci la tua password</label>
            </div>
            <button type="submit" class="btn text-white w-100 rounded mt-5 text-uppercase"> Accedi </button>
            <p class="text-center text-secondary text-center mt-5">Non hai un'account? Clicca <a class="text-transform-underline text-secondary" routerLink="/registration" >QUI</a> per registrarti</p>
          </form>
        </div>
      </div>
    </div>
  </main>
  `,
  styles: [`
    .row{
      height:85vh;
    }
    input{
      border:none;
      border-radius:0px;
      background:#111111;
    }
    input.form-control{
      color:white;
    }
    input ::placeholder{
      color:white;
    }
    button{
      background:#64737c;
    }
    input:focus{
      outline: none !important;
      box-shadow: 0 0 0 transparent;
      background:#111111;
    }
  `]
})
export class LoginComponent implements OnInit {

  model ={
    email :'',
    password:''
  };

  error: any = null;


  constructor(private _loginService: LoginService,private router : Router) { }

  ngOnInit() {
    if(this._loginService.isLoggedIn())
    this.router.navigateByUrl('/');
  }

  onSubmit(form : NgForm){
    //console.log(form.value);
    this._loginService.login(form.value).subscribe(
      res => {
        
        //console.log(res['token']);
        this._loginService.setToken(res['token']);
        //console.log(res['token']);
        this.router.navigateByUrl('/');
        window.location.reload();
      },
      err => {
        form.reset()
        this.error = err
        Swal.fire('OOPS :(', err.error.message, 'error')
      }
    );
  }

}
