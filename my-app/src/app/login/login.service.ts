import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private baseUrl: string = "http://localhost:4000/users";
  noAuthHeader = { headers: new HttpHeaders({ 'NoAuth': 'True' }) };

  constructor(private _httpClient: HttpClient) { };

  login(authCredentials) {
    return this._httpClient.post(this.baseUrl + '/login', authCredentials, this.noAuthHeader);
  }

  getUserPayload() {
    var token = this.getToken();
    if (token) {
      var userPayload = atob(token.split('.')[1]);
      return JSON.parse(userPayload);
    }
    else
      return null;
  }

  isLoggedIn() {
    var userPayload = this.getUserPayload();
    if (userPayload)
      return userPayload.exp > Date.now() / 1000;
    else
      return false;
  }

 setToken(token: string) {
    localStorage.setItem('token', token);
    
    console.log(this.getToken());
  }

  getToken() {
    //console.log(localStorage.getItem('token'))
    return localStorage.getItem('token');
  }

  deleteToken() {
    localStorage.removeItem('token');
    //console.log(this.getToken());
  }
}
