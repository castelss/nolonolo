import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdmAllOrdersService } from './adm-all-orders.service';
import { Rent } from '../../../../backend/models/rent';

@Component({
  selector: 'app-adm-all-orders',
  template: `
  <main>
    <div class="container-fluid container-lg">
    <h3 class="text-white mt-5">Storico e futuro dei noleggi</h3>
    <div class="form-group w-100 mt-4">
        <input type="text" class="form-control border-bottom border-bottom" placeholder="Filtra gli ordini.." [(ngModel)]="filterTerm">
      </div>
      <div class="row row-cols-1 row-col-sm-2 row-col-md-2 row-col-lg-3 mt-3">
      <div class="table-responsive w-100">
      <table class="table table-light">
          <thead>
          <tr>
            <th scope="col">Identificativo</th>
            <th scope="col">Inizio</th>
            <th scope="col">Fine</th>
            <th scope="col">Cliente</th>
            <th scope="col">Videogioco</th>
            <th scope="col">Stato</th>
            <th scope="col">Modifica</th>
          </tr>
        </thead>
        <tbody>
          <tr *ngFor="let rent of rents  | filter:filterTerm">
            <td>{{rent._id}}</td>
            <td>{{rent.dataInizio | date}}</td>
            <td>{{rent.dataFine | date}}</td>
            <td>{{rent.user.nome}} {{rent.user.cognome}} </td>
            <td>{{rent.videogioco.nome}}</td>
            <td >{{rent.stato}} <i class="bi bi-circle-fill"></i></td>
            <td><a routerLink="./order_mgmt/{{rent._id}}"><i class="bi bi-arrow-right"></i></a></td>
          </tr>
        </tbody>
      </table>
      <p *ngIf="!(rents | filter:filterTerm).length" class="text-white">Non ci sono ancora noleggi da visualizzare.</p>
    </div>
      </div>
    </div>
  </main>
  `,
  styles: [
    `
    
    
    .wrapper{
      background:black;
      height: 12vh; 
      z-index: 99;
    }
    input{
      outline:none !important;
      border:none;
      background:#111111;
      border-radius:0px;
    }
    ::placeholder {
      color: white;
    }
    input.form-control{
      color:white;
      font-size:12px;
    }
    .form-group{
      width:80%;
    }
    input:focus{
      outline:none !important;
      box-shadow: 0 0 0 transparent;
      background:#111111;
    }
    .container-fluid{
      padding-top: 5vh;
    }
    .card{
      background:#111111;
    }

    `
  ]
})
export class AdmAllOrdersComponent implements OnInit {
  filterTerm;

  rents : any = []
  constructor(private router : Router, private _allOrdesService : AdmAllOrdersService) { }

  ngOnInit(): void {
    this._allOrdesService.getRents().subscribe(rents=>{
      this.rents = rents
    })
  }

}
