import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from '../login/login.service';
import { VideogameService } from '../videogame/videogame.service';
import { Rent } from '../../../../backend/models/rent'
import { UserOrdersService } from '../user-orders/user-orders.service';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { OrderDetailsService } from './order-details.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-order-details',
  template: `
  <main class="container">
    <h1 class="text-white mb-5 mt-5 px-3">I dettagli che hai chiesto sul noleggio</h1> 
    <div class="row row-cols-1 row-cols-sm-1 row-cols-md-2 row-cols-lg-3 ">
      <div class="col-md-4 p-4">
        <div class="border h-100">
          <div class="border-bottom" style="height: 5vh" *ngIf="rent != undefined">
            <h6 class="text-white px-3" style="line-height: 5vh">Noleggio</h6>
          </div>
          <div class="table-responsive px-2 mt-3">
            <table class="table text-white table-borderless">
              <tbody>
                <tr>
                  <th scope="row">Identificativo</th>
                  <td>{{rent._id}}</td>
                </tr>
                <tr>
                  <th scope="row">Data di richiesta</th>
                  <td>{{rent.createdAt | date}}</td>
                </tr>
                <tr>
                  <th scope="row">Data di inizio noleggio</th>
                  <td>{{rent.dataInizio | date}}</td>
                </tr>
                <tr>
                  <th scope="row">Data di fine noleggio</th>
                  <td>{{rent.dataFine | date}}</td>
                </tr>
                <tr>
                  <th scope="row">Stato del noleggio</th>
                  <td>{{rent.stato}}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-md-4 second p-4">
        <div class="border h-100">
          <div class="border-bottom" style="height: 5vh">
            <h6 class="text-white px-3" style="line-height: 5vh">Videogioco</h6>
          </div>
          <div class="table-responsive px-2 mt-3">
            <table class="table text-white table-borderless">
              <tbody>
                <tr>
                  <th scope="row">Nome</th>
                  <td>{{rent.videogioco.nome}}</td>
                </tr>
                <tr>
                  <th scope="row">Conservazione</th>
                  <td>{{rent.videogioco.conservazione}}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-md-4 third p-4">
        <div class="border h-100">
          <div class="border-bottom" style="height: 5vh">
            <h6 class="text-white px-3" style="line-height: 5vh">Pagamento</h6>
          </div>
          <div class="table-responsive px-2 mt-3">
            <table class="table text-white table-borderless">
              <tbody>
                <tr>
                  <th scope="row">Modalità</th>
                  <td>Pagamento in negozio</td>
                </tr>
                <tr>
                  <th scope="row">Luogo</th>
                  <td>Nolonolo Bologna</td>
                </tr>
                <tr>
                  <th scope="row">Stato</th>
                  <td *ngIf="rent.stato != 'pagato'"> Non disponibile</td>
                  <td *ngIf="rent.stato == 'pagato'"> Pagato</td>
                </tr>
                <tr>
                  <th scope="row">Data</th>
                  <td class="text-white" *ngIf="rent.fattura.dataPagamento == null" class="fw-bold">Non ancora disponibile</td> <hr>
                  <td class="text-white" *ngIf="rent.fattura.dataPagamento != null">{{rent.fattura.dataPagamento | date}}</td> <hr>
                </tr>
              </tbody>
            </table>
          </div>
        </div>  
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 fourth p-4">
        <h5 class="text-white">Note dello staff</h5>
        <p class="text-white">{{rent.fattura.nota}}</p>
      </div>
      <div class="col-lg-12 fifth p-4">
        <h5 class="text-white" *ngIf="rent.fattura.totale == 0">€ {{rent.prezzoPrevisto | number : '1.2-2'}}</h5> 
        <h5 class="text-white" *ngIf="rent.fattura.totale != 0">€ {{rent.fattura.totale | number : '1.2-2'}}</h5> <hr>
        <table class="table text-white table-borderless">
          <tbody>
            <tr>
              <th scope="row">Subtotale</th>
              <td>€ {{rent.prezzoPrevisto | number : '1.2-2' }}</td>
            </tr>
            <tr>
              <th scope="row">Sconti</th>
              <td *ngIf="rent.fattura.sconto == 0" class="fw-bold">Non ancora disponibile</td>
              <td *ngIf="rent.fattura.sconto != 0">- {{rent.fattura.sconto | number : '1.2-2'}} €</td>
            </tr>
            <tr>
              <th scope="row">More</th>
              <td *ngIf="rent.fattura.mora == 0" class="fw-bold">Non ancora disponibile</td>
              <td *ngIf="rent.fattura.mora != 0">- {{rent.fattura.mora | number : '1.2-2'}} €</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <!--SE LA DATA DI INIZIO è INFERIORE A QUELLA ATTUALE, IL CLIENTE PUO CANCELLARE IL NOLEGGIO O MODIFICARE LE DATE-->

    <div class="row d-flex align-items-center px-2 py-3">
      <h2 class="text-white">Ecco cosa puoi fare</h2>
      <div class="mt-3">
        <button type="button" class="btn text-white  rounded-pill" (click)="deleteRent()">Vorrei cancellare la prenotazione</button>
        <button type="button" class="btn text-white  rounded-pill ms-md-3 mt-3 mt-md-0" data-bs-toggle="modal" data-bs-target="#exampleModal">Vorrei modificare la prenotazione</button>
      </div>
    </div>


    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog  modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Cambia le date della tua prenotazione</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <form class="d-flex flex-column justify-content-center" (ngSubmit)="isAvailable()" [formGroup]="updateRent">
              <div class="mb-3 row">
                <label for="staticEmail" class="col-sm-6 col-form-label">Id prenotazione: </label>
                <div class="col-sm-6">
                  <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{rent._id}}" formControlName="id">
                </div>
              </div>
              <label for="inputPassword5" class="form-label">Nuova data di inizo</label>
              <input type="date" id="inputPassword5" class="form-control" aria-describedby="passwordHelpBlock" formControlName="dataInizio">
              <div id="passwordHelpBlock" class="form-text mb-4">
                Attenzione! La data che scegli deve essere antecedente alla data di oggi
              </div>
              <label for="inputPassword5" class="form-label">Nuova data di fine</label>
              <input type="date" id="inputPassword5" class="form-control" aria-describedby="passwordHelpBlock" formControlName="dataFine">
              <div id="passwordHelpBlock" class="form-text">
                Attenzione! La data che scegli deve essere successiva alla nuova data di inizo
              </div>
              <button type="submit" class="btn text-center text-white mt-4 ">Prenota</button>
            </form>
           </div>
        </div>
      </div>
    </div>
  </main>
  `,
  styles: [
    
    `
    button{
      background:#64737c;
    }
    main{
      padding-top:60px;
    }
    hr{
      color:white;
    }
    button{
      border:none;
    }
    table{
      font-size: 10px;
    }
    table th{
      text-transform:uppercase;
    }
    button{
      width:100%;
    }
    button:focus{
      outline:none !important;
      box-shadow: 0 0 0 transparent;
    }
    @media(min-width: 576px){
      main{
        padding-top: 100px;
      }
      button{
        width:30%;
      }

    }
    `
  ]
})
export class OrderDetailsComponent implements OnInit {

  
  isShown_1: boolean = false ;
  isShown_2: boolean = false ;

  getRentalForm(){
    this.isShown_1 = ! this.isShown_1;
  }
  getReviewForm(){
    this.isShown_2 = ! this.isShown_2;
  }
  id : any = ''
  rent: Rent = {}
  
  updateRent : FormGroup
  

  constructor(private _orderDetailsService: OrderDetailsService,
    private _userOrderService : UserOrdersService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private _loginService: LoginService, private formBuilder : FormBuilder) { 
      this.rent.videogioco = {}
      this.rent.user = {}
      this.rent.fattura={}

      this.updateRent = this.formBuilder.group({id:"",dataInizio : "",dataFine: "" })

    }

  ngOnInit(): void {
    
    this.id = this.activatedRoute.snapshot.params['id']

    if(this.id){
      this._userOrderService.getRent(this.id).subscribe(
        res => {
          this.rent = res['rent'];
          console.log(this.rent.user)
        },
        err => { 
          alert("dettagli non disponibili");
          this.router.navigateByUrl('/user_area')
        }
      )
    }
  }

  isAvailable(){
    this.updateRent.value['id'] = this.rent._id
    this._orderDetailsService.modifica(this.updateRent.value).subscribe(
      res => {
        Swal.fire('OTTIMO :)', "Noleggio modificato con successo", 'success')  
        this.router.navigateByUrl('/my_rentals');
          
      },
      err => {
        Swal.fire('OOPS :(', "Date non disponibili, prova a cambiarle", 'error')
        this.updateRent.reset()
      }
    )

  }
  deleteRent(){
    this._orderDetailsService.delete(this.rent).subscribe(
      res => {
        Swal.fire('OTTIMO :)', "Noleggio cancellato con successo", 'success')
          this.router.navigateByUrl('/my_rentals');
      },
      err => {
        Swal.fire('OOPS :(', "Errore interno al server, impossibile cancellare", 'error')
        this.updateRent.reset()
      }
    )
  }
}
