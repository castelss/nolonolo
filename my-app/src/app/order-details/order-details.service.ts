import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OrderDetailsService {
  constructor(private _httpClient: HttpClient) { };
  private baseUrl: string = "http://localhost:4000/noleggi/";
  modifica(form){
    return this._httpClient.post(this.baseUrl+"modifica",form)
  }

  delete(rent){
    return this._httpClient.post(this.baseUrl+"delete",rent)
  }
}
